package ar.com.kiritsu.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public interface Renderer<T> {

	void render(GameContainer container, Graphics g, T value) throws SlickException;

}
