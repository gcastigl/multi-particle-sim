package ar.com.kiritsu.math;

import java.util.Collection;

import com.google.common.base.Preconditions;

public class FloatUtil {

	public static float sum(Collection<Float> values) {
		float sum = 0;
		for (float value : values) {
			sum += value;
		}
		return sum;
	}

	public static float avg(Collection<Float> values) {
		Preconditions.checkArgument(!values.isEmpty());
		return sum(values) / values.size();
	}

}
