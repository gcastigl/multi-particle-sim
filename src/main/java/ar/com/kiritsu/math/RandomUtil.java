package ar.com.kiritsu.math;

import org.newdawn.slick.geom.Vector2f;

import com.google.common.base.Preconditions;

public class RandomUtil {

	private static final Vector2f zeroVcetor = new Vector2f();

	public static final float random(float min, float max) {
		Preconditions.checkArgument(min <= max);
		return (float) (Math.random() * (max - min)) + min;
	}

	public static final Vector2f randomVector(Vector2f ubound, Vector2f output) {
		return randomVector(zeroVcetor, ubound, output);
	}

	public static final Vector2f randomVector(Vector2f lbound, Vector2f ubound, Vector2f output) {
		float x = random(lbound.x, ubound.x);
		float y = random(lbound.y, ubound.y);
		output.set(x, y);
		return output;
	}
}
