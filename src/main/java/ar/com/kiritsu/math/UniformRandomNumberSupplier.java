package ar.com.kiritsu.math;

import com.google.common.base.Supplier;

public class UniformRandomNumberSupplier implements Supplier<Float> {

	private float _n;

	public UniformRandomNumberSupplier(float n) {
		_n = n;
	}

	@Override
	public Float get() {
		return RandomUtil.random(-_n, _n);
	}

}
