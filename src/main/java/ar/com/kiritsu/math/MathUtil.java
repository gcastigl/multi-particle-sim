package ar.com.kiritsu.math;

import com.google.common.base.Preconditions;

public class MathUtil {

	public static final float capped(float value, float min, float max) {
		return Math.max(min, Math.min(max, value));
	}

	public static final float norm2(float x, float y) {
		return (float) Math.sqrt(x * x + y * y);
	}

	public static final float norm2(float x, float y, float z) {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public static final int factorial(int n) {
		Preconditions.checkArgument(n >= 0);
		int result = 1;
		for (int i = 2; i <= n; i++) {
			result *= i;
		}
		return result;
	}

	public static final int[] factorial(int[] values) {
		for (int i = 0; i < values.length; i++) {
			values[i] = factorial(values[i]);
		}
		return values;
	}
}
