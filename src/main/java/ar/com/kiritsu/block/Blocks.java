package ar.com.kiritsu.block;

import java.util.Collection;

import com.google.common.base.Function;

public class Blocks {

	public static <F> Function<Function<F, ? extends Object>, Void> foreach(Collection<F> elems) {
		return new ForEach<F>(elems);
	}

	private static class ForEach<F> implements Function<Function<F, ? extends Object>, Void> {
		private Collection<F> _elems;

		public ForEach(Collection<F> elems) {
			_elems = elems;
		}

		@Override
		public Void apply(Function<F, ? extends Object> input) {
			for (F elem : _elems) {
				input.apply(elem);
			}
			return null;
		}
	}
}
