package ar.com.kiritsu.block;

import com.google.common.base.Function;

public abstract class Block<T> implements Function<T, T> {

	@Override
	public final T apply(T input) {
		exec(input);
		return input;
	}

	protected abstract void exec(T input);

}
