package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

public class SegmentIntersection {

	private Vector2f _intersection = new Vector2f();

	public Vector2f at(Line s1, Line s2) {
		Vector2f start1 = s1.getStart();
		Vector2f end1 = s1.getEnd();
		Vector2f start2 = s2.getStart();
		Vector2f end2 = s2.getEnd();
		Vector2f ans = at(start1.x, start1.y, end1.x, end1.y, start2.x, start2.y, end2.x, end2.y); 
		return ans;
	}

	private Vector2f at(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y) {
		float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
		s10_x = p1_x - p0_x;
		s10_y = p1_y - p0_y;
		s32_x = p3_x - p2_x;
		s32_y = p3_y - p2_y;

		denom = s10_x * s32_y - s32_x * s10_y;
		if (denom == 0)
			return null; // Collinear
		boolean denomPositive = denom > 0;

		s02_x = p0_x - p2_x;
		s02_y = p0_y - p2_y;
		s_numer = s10_x * s02_y - s10_y * s02_x;
		if ((s_numer < 0) == denomPositive)
			return null; // No collision

		t_numer = s32_x * s02_y - s32_y * s02_x;
		if ((t_numer < 0) == denomPositive)
			return null; // No collision

		if (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive))
			return null; // No collision
		// Collision detected
		t = t_numer / denom;
		_intersection.x = p0_x + (t * s10_x);
		_intersection.y = p0_y + (t * s10_y);
		return _intersection;
	}
}
