package ar.com.kiritsu.multiparticlesim.tp2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.math.UniformRandomNumberSupplier;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.AreaRender;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.IncrementElapsedTime;
import ar.com.kiritsu.multiparticlesim.log.LogAreaToFile;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

public class FlockingBehaviorApp extends BasicGame {

	public static void main(String[] args) throws SlickException {
		FlockingAppConfiguration config = new FlockingAppConfiguration();
		config
			.setwindowSize(700)
			.setTimeStep(1 / 10f)
			.setN(300)
			.setL(25)
			.setETA(0.1f)
			.setVelocity(0.3f)
			.setCellsCount(10)
			.setNeighborDistance(0.5f)
			.setTrailSize(1)
			.setDynamicFile("src/main/resources/dynamic.txt")
			.setAreaFile("src/main/resources/area.txt")
			;
		AppGameContainer appContainer = new AppGameContainer(new FlockingBehaviorApp(config));
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(config.windowSize(), config.windowSize(), false);
		appContainer.start();
	}

	private final FlockingAppConfiguration _config;
	private AreaRender _areaRender;
	private Area _area;
	private Function<Area, Area> _step;

	private Writer _dynamicWriter, _areaWriter;

	public FlockingBehaviorApp(FlockingAppConfiguration config) {
		super("Flocking behavior demo");
		_config = config;
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		container.setTargetFrameRate(50);
		float scale = _config.windowSize() / _config.L();
		_areaRender = new AreaRender().setScale(scale);//_config.L() / 100f
		_area = new Area().setBounds(new Vector2f(_config.L(), _config.L()));
		FindNeighbors neighborFinder = new FindNeighbors(_config.cellsCount()).atDistance(_config.neighborDistance());
		Supplier<Float> randomSupplier = new UniformRandomNumberSupplier(_config.ETA() / 2);
		first(new EmitParticles(_config.N()).withDesiredVelocity(_config.velocity()))
			.then(new TraceParticleSteps().setTrailSize(_config.trailSize()).setDelaySteps(2))
			.then(new StepParticles(neighborFinder, randomSupplier, _config.timeStep()))
			.then(new MirrorOutOfBoundsParticles())
			.then(logToFile())
			.then(new IncrementElapsedTime(Suppliers.ofInstance(_config.timeStep())))
			;
	}

	private FlockingBehaviorApp first(Function<Area, Area> f) {
		_step = f;
		return this;
	}

	private FlockingBehaviorApp then(Function<Area, Area> g) {
		_step = Functions.compose(g, _step);
		return this;
	}

	private Function<Area, Area> logToFile() {
		return new LogAreaToFile( 
			_dynamicWriter = buildWriter(_config.dynamicFile()),
			_areaWriter = buildWriter(_config.dynamicFile())
		);
	}
	
	private Writer buildWriter(String file) {
		try {
			return new BufferedWriter(new FileWriter(file));
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		_areaRender.render(container, g, _area);
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		_area = _step.apply(_area);
		if (container.getInput().isKeyDown(Input.KEY_R)) {
			container.reinit();
		}
		if (container.getInput().isKeyDown(Input.KEY_ESCAPE)) {
			try {
				_dynamicWriter.close();
				_areaWriter.close();
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
			container.exit();
		}
	}

}
