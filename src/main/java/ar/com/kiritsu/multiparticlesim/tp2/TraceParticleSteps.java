package ar.com.kiritsu.multiparticlesim.tp2;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Function;

public class TraceParticleSteps implements Function<Area, Area> {

	private int _trailSize = 1;
	private int delaySteps = 2;
	private int steps;

	public TraceParticleSteps setTrailSize(int trailSize) {
		this._trailSize = trailSize;
		return this;
	}

	public TraceParticleSteps setDelaySteps(int delaySteps) {
		this.delaySteps = delaySteps;
		return this;
	}

	@Override
	public Area apply(Area input) {
		steps++;
		if (steps >= delaySteps) {
			steps = 0;
			for (Particle particle : input.particles()) {
				if (particle.steps().size() >= _trailSize) {
					particle.steps().remove(0);
				}
				particle.addStep(particle.position().copy());
			}
		}
		return input;
	}

}
