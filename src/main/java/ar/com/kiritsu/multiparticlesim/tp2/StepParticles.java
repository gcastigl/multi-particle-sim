package ar.com.kiritsu.multiparticlesim.tp2;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import java.util.Collection;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

public class StepParticles implements Function<Area, Area> {

	private Vector2f _position = new Vector2f();
	private Vector2f _velocity = new Vector2f();
	private FindNeighbors _findNeighbors;
	private Supplier<Float> _deltaDegrees;
	private float _timeStep;

	public StepParticles(FindNeighbors findNeighbors, Supplier<Float> deltaDegrees, float timeStep) {
		_findNeighbors = findNeighbors;
		_deltaDegrees = deltaDegrees;
		_timeStep = timeStep;
	}

	@Override
	public Area apply(Area input) {
		Area updatedArea = input.copy();
		_findNeighbors.at(input);
		for (Particle particle : input.particles()) {
			Collection<Particle> neighbors = _findNeighbors.centeredOn(particle).find();
			neighbors.add(particle); // Self must taken in count as a neighbor in this context
			updatedArea.addParticle(particle.copy()
				.setVelocity(deltaVelocity(neighbors, particle.expectedVelocity()))
				.setPosition(deltaPosition(particle))
			);
		}
		return updatedArea;
	}

	private Vector2f deltaVelocity(Collection<Particle> neighbors, float velocityModule) {
		float avgCos = 0;
		float avgSin = 0;
		for (Particle neighbor : neighbors) {
			float theta = (float) toRadians(neighbor.velocity().getTheta());
			avgCos += cos(theta);
			avgSin += sin(theta);
		}
		avgSin /= neighbors.size();
		avgCos /= neighbors.size();
		_velocity.set(1, 0);
		_velocity.setTheta(toDegrees(atan2(avgSin, avgCos)) + _deltaDegrees.get());
		return _velocity.scale(velocityModule * _timeStep);
	}

	private Vector2f deltaPosition(Particle particle) {
		_position.set(particle.position());
		_position.add(_velocity);
		return _position;
	}

}
