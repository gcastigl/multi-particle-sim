package ar.com.kiritsu.multiparticlesim.tp2;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Function;

public class MirrorOutOfBoundsParticles implements Function<Area, Area> {

	@Override
	public Area apply(Area input) {
		Vector2f bounds = input.bounds();
		for (Particle particle : input.particles()) {
			Vector2f position = particle.position();
			position.x = mirror(position.x, bounds.x);
			position.y = mirror(position.y, bounds.y);
		}
		return input;
	}

	private float mirror(float n, float bounds) {
		if (n < 0) {
			n += bounds;
		} else if (n >= bounds) {
			n -= bounds - 2 * Float.MIN_VALUE;
		}
		return n;
	}
}
