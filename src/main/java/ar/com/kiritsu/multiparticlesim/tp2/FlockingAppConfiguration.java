package ar.com.kiritsu.multiparticlesim.tp2;

public class FlockingAppConfiguration {

	private int _windowSize;
	private float _timeStep;
	private int _N;
	private float _L;
	private float _ETA;
	private float _velocity;
	private int _cellsCount;
	private float _neighborDistance;
	private int _trailSize;

	private String _dynamicFile, _areaFile;

	public int windowSize() {
		return _windowSize;
	}

	public FlockingAppConfiguration setwindowSize(int windowSize) {
		_windowSize = windowSize;
		return this;
	}

	public float timeStep() {
		return _timeStep;
	}

	public FlockingAppConfiguration setTimeStep(float timeStep) {
		_timeStep = timeStep;
		return this;
	}

	public int N() {
		return _N;
	}

	public FlockingAppConfiguration setN(int n) {
		_N = n;
		return this;
	}

	public float L() {
		return _L;
	}

	public FlockingAppConfiguration setL(float l) {
		_L = l;
		return this;
	}

	public float ETA() {
		return _ETA;
	}

	public FlockingAppConfiguration setETA(float eTA) {
		_ETA = eTA;
		return this;
	}

	public float velocity() {
		return _velocity;
	}

	public FlockingAppConfiguration setVelocity(float velocity) {
		_velocity = velocity;
		return this;
	}

	public int cellsCount() {
		return _cellsCount;
	}

	public FlockingAppConfiguration setCellsCount(int cellsCount) {
		_cellsCount = cellsCount;
		return this;
	}

	public float neighborDistance() {
		return _neighborDistance;
	}

	public FlockingAppConfiguration setNeighborDistance(float neighborDistance) {
		_neighborDistance = neighborDistance;
		return this;
	}

	public int trailSize() {
		return _trailSize;
	}

	public FlockingAppConfiguration setTrailSize(int trailSize) {
		_trailSize = trailSize;
		return this;
	}

	public FlockingAppConfiguration setDynamicFile(String dynamicFile) {
		this._dynamicFile = dynamicFile;
		return this;
	}

	public String dynamicFile() {
		return _dynamicFile;
	}
	
	public FlockingAppConfiguration setAreaFile(String _areaFile) {
		this._areaFile = _areaFile;
		return this;
	}
	
	public String areaFile() {
		return _areaFile;
	}
}
