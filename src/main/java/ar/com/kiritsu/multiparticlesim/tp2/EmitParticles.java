package ar.com.kiritsu.multiparticlesim.tp2;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.math.RandomUtil;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Function;

public class EmitParticles implements Function<Area, Area> {

	private int _minParticles;
	private int lastId;
	private float _desiredVelocity;
	
	private final Vector2f output = new Vector2f();
	private final Vector2f minVelocity = new Vector2f(-1, -1);
	private final Vector2f maxVelocity = new Vector2f(1, 1);

	public EmitParticles(int minParticles) {
		_minParticles = minParticles;
		lastId = 0;
	}

	public EmitParticles withDesiredVelocity(float desiredVelocity) {
		_desiredVelocity = desiredVelocity;
		return this;
	}
	
	@Override
	public Area apply(Area input) {
		if (input.particles().size() >= _minParticles) {
			return input;
		}
		int delta = _minParticles - input.particles().size();
		for (int i = 0; i < delta; i++) {
			Particle born = new Particle(lastId++, 0);
			born
				.setExpectedVelocity(_desiredVelocity)
				.setPosition(RandomUtil.randomVector(input.bounds(), output))
				.setVelocity(RandomUtil.randomVector(minVelocity, maxVelocity, output));
			input.particles().add(born);
		}
		return input;
	}

}
