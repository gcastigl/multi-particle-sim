package ar.com.kiritsu.multiparticlesim;

import java.util.Collection;
import java.util.Iterator;

import org.newdawn.slick.geom.Vector2f;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;

public class Vectors2f {

	private static final Vector2f X = new Vector2f(1, 0);
	private static final Vector2f ZERO = new Vector2f(0, 0);
	private static final Vector2f ONES = new Vector2f(1, 1);

	public static Vector2f X() {
		Preconditions.checkArgument(X.x == 1 && X.y == 0, "Invalid X versor");
		return X;
	}

	public static Vector2f ZERO() {
		Preconditions.checkArgument(ZERO.x == 0 && ZERO.y == 0, "Invalid NULL versor");
		return ZERO;
	}

	public static Vector2f ONES() {
		Preconditions.checkArgument(ONES.x == 1 && ONES.y == 1, "Invalid ONES versor");
		return ONES;
	}

	public static Function<Vector2f, Vector2f> copy() {
		return new Function<Vector2f, Vector2f>() {
			@Override
			public Vector2f apply(Vector2f input) {
				return new Vector2f(input);
			}
		};
	}

	public static void versor(Vector2f v1, Vector2f v2, Vector2f cache) {
		cache.set(v2);
		cache.sub(v1);
		cache.normalise();
	}

	private static final Vector2f TMP_VECTOR = new Vector2f();

	public static void add(Vector2f v1, Vector2f direction, float amount, Vector2f cache) {
		cache.set(v1);
		TMP_VECTOR.set(direction);
		cache.add(TMP_VECTOR.scale(amount));
	}

	public static float angle(Vector2f v1, Vector2f v2) {
		return (float) Math.toRadians(v2.getTheta() - v1.getTheta());
	}

	public static Vector2f scale(Vector2f v, float c) {
		return v.copy().scale(c);
	}

	public static Vector2f add(Vector2f... vs) {
		Vector2f result = new Vector2f();
		for (Vector2f v : vs) {
			result.add(v);
		}
		return result;
	}

	public static Vector2f sub(Vector2f v1, Vector2f v2) {
		return v1.copy().sub(v2);
	}

	public static Vector2f avg(Collection<Vector2f> vs) {
		Vector2f result = new Vector2f();
		for (Vector2f v : vs) {
			result.add(v);
		}
		return result.scale(1f / vs.size());
	}

	public static Vector2f avgWeightened(Collection<Vector2f> vs, Collection<Float> ws) {
		Preconditions.checkArgument(vs.size() == ws.size());
		Vector2f result = new Vector2f();
		Vector2f tmp = new Vector2f();
		float totalW = 0;
		Iterator<Float> wsIt = ws.iterator();
		Iterator<Vector2f> vsIt = vs.iterator();
		while (wsIt.hasNext()) {
			Float w = wsIt.next();
			tmp.set(vsIt.next());
			result.add(tmp.scale(w));
			totalW += w;
		}
		return result.scale(1 / totalW);
	}

	public static void multiply(float[] ws, Vector2f[] vs, Vector2f result) {
		Preconditions.checkArgument(vs.length == ws.length);
		result.set(0, 0);
		Vector2f tmp = new Vector2f();
		for (int i = 0; i < vs.length; i++) {
			tmp.set(vs[i]);
			tmp.scale(ws[i]);
			result.add(tmp);
		}
	}
}
