package ar.com.kiritsu.multiparticlesim.log;

import java.util.List;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import com.google.common.collect.Lists;

public class AreaFile {

	private Vector2f _size;
	private List<Line> _walls = Lists.newLinkedList();

	public AreaFile(Vector2f size) {
		_size = size;
	}

	public Vector2f size() {
		return _size;
	}

	public List<Line> walls() {
		return _walls;
	}

	public void addWall(Line walls) {
		_walls.add(walls);
	}
}
