package ar.com.kiritsu.multiparticlesim.log;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Wall;

public class LogAreaToFile extends Block<Area> {

	private static final String ENTER = "\n";
	private static final String TAB = "\t";
	
	private Writer _dynamicFile, _areaFile;
	private boolean initialized;
	private final int _logeEveryNFrames;
	private int _skipped;

	public LogAreaToFile(Writer dynamicFile, Writer areaFile) {
		this(dynamicFile, areaFile, 0);
	}

	public LogAreaToFile(Writer dynamicFile, Writer areaFile, int logeEveryNFrames) {
		_dynamicFile = dynamicFile;
		_areaFile = areaFile;
		initialized = false;
		_logeEveryNFrames = logeEveryNFrames;
		_skipped = 0;
	}

	@Override
	public void exec(Area input) {
		try {
			logToStaticFile(input);
			_skipped++;
			if (_skipped > _logeEveryNFrames) {
				_skipped = 0;
				logToDynamicFile(input);
			}
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}

	private void logToStaticFile(Area area) throws IOException {
		if (!initialized) {
			initialized = true;
			logArea(area);
		}
//		if (_declaredParticleIds.size() != area.particles().size()) {
//			for (Particle particle : filter(area.particles(), not(isDeclared))) {
//				_staticFile.write(particle.id() + TAB + formatted(particle.radius()) + TAB + particle.group() + ENTER);
//				_declaredParticleIds.add(particle.id());
//			}
//		}
	}

	private void logArea(Area input) throws IOException {
//		_staticFile.write(input.particles().size() + ENTER);
//		_staticFile.write(formatted(input.bounds().x) + ENTER);
		_areaFile.write("width = " + formatted(input.bounds().x) + ENTER);
		_areaFile.write("height = " + formatted(input.bounds().y) + ENTER);
		StringBuffer walls = new StringBuffer("walls = ");
		for (Wall wall : input.obstacles()) {
			Line bounds = wall.bounds();
			Vector2f start = bounds.getStart();
			Vector2f end = bounds.getEnd();
			walls.append(formatted(start.x) + " " + formatted(start.y));
			walls.append(" " + formatted(end.x) + " " + formatted(end.y));
			walls.append(", ");
		}
		_areaFile.write(walls.toString());
	}

	private void logToDynamicFile(Area area) throws IOException {
		if (area.particles().size() == 0) {
			return;
		}
		_dynamicFile.write(Float.toString(area.elapsedTime()) + TAB + area.particles().size() + ENTER);
		for (Particle particle : area.particles()) {
			_dynamicFile.write(
				particle.id() + TAB + particle.group() + TAB + particle.radius() + TAB + particle.mass()
				+ TAB + asString(particle.position()) 
				+ TAB + asString(particle.velocity()) + ENTER);
		}
	}

	private String asString(Vector2f vector) {
		return formatted(vector.x) + TAB + formatted(vector.y);
	}

	private String formatted(float n) {
		return new BigDecimal(n).setScale(3, RoundingMode.DOWN).toString();
	}
}
