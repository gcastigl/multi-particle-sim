package ar.com.kiritsu.multiparticlesim.log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import com.google.common.base.Strings;

public class AreaFileParser {

	public AreaFile parse(InputStream is) throws IOException {
		Properties properties = new Properties();
		properties.load(is);
		float x = Float.valueOf(properties.getProperty("width").trim());
		float y = Float.valueOf(properties.getProperty("height").trim());
		AreaFile areaFile = new AreaFile(new Vector2f(x, y));
		String[] walls = Strings.nullToEmpty(properties.getProperty("walls")).split(",");
		for (String wall : walls) {
			wall = wall.trim();
			if (wall.isEmpty()) {
				continue;
			}
			String[] values = wall.split(" ");
			float x1 = Float.valueOf(values[0]);
			float y1 = Float.valueOf(values[1]);
			float x2 = Float.valueOf(values[2]);
			float y2 = Float.valueOf(values[3]);
			areaFile.addWall(new Line(new Vector2f(x1, y1), new Vector2f(x2, y2)));
		}
		return areaFile;
	}
}
