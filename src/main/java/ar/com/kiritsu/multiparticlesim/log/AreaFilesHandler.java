package ar.com.kiritsu.multiparticlesim.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import org.newdawn.slick.geom.Line;

import ar.com.kiritsu.multiparticlesim.log.StaticFile.ParticleDesc;

import com.google.common.base.Supplier;

public class AreaFilesHandler {

	private static final String SEPARATOR = "\t";

	private static final DynamicFileParser dynamicParser = new DynamicFileParser(SEPARATOR);

	private Scanner _dynamicFileScanner;
	private AreaFile _areaFile;
	private Supplier<DynamicFileStep> _steps;

	// Maintened for retro-compatibility
	public AreaFilesHandler(File _, File dynamicFile, File areaFile) throws IOException {
		this(dynamicFile, areaFile, 0);
	}

	public AreaFilesHandler(File dynamicFile, File areaFile, int skipFrames) throws IOException {
		_dynamicFileScanner = new Scanner(dynamicFile);
		_steps = dynamicParser.parse(_dynamicFileScanner, skipFrames);
		InputStream is = new FileInputStream(areaFile);
		_areaFile = new AreaFileParser().parse(is);
		is.close();
	}

	public Supplier<DynamicFileStep> steps() {
		return _steps;
	}

	public DynamicFileStep nextSteps() {
		return _steps.get();
	}

	public float areaSize() {
		return _areaFile.size().x;
	}

	@Deprecated
	public List<ParticleDesc> particlesDesc() {
		return null;
	}

	public void close() {
		_dynamicFileScanner.close();
	}

	public List<Line> getWallsDef() {
		return _areaFile.walls();
	}
}
