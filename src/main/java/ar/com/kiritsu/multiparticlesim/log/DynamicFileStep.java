package ar.com.kiritsu.multiparticlesim.log;

import java.util.List;

import org.newdawn.slick.geom.Vector2f;

import com.google.common.collect.Lists;

public class DynamicFileStep {

	private final int _index;
	private float _startTime, _duration;
	private List<ParticleStatus> _particles = Lists.newArrayList();

	public DynamicFileStep(float startTime, float duration, int index) {
		_startTime = startTime;
		_index = index;
		setDuration(duration);
	}

	public List<ParticleStatus> particles() {
		return _particles;
	}

	public boolean containsId(int id) {
		for (ParticleStatus status : particles()) {
			if (status.id() == id) {
				return true;
			}
		}
		return false;
	}

	public float startTime() {
		return _startTime;
	}

	public float endTime() {
		return startTime() + duration();
	}

	public void setDuration(float duration) {
		_duration = duration;
	}

	public float duration() {
		return _duration;
	}
	
	public int index() {
		return _index;
	}

	public static final class ParticleStatus {
		private int _id;
		private int _group;
		private float _radius;
		private float _mass;
		private Vector2f _center;
		private Vector2f _velocity;

		public ParticleStatus(int id, int group, float radiud, float mass, Vector2f center, Vector2f velocity) {
			_id = id;
			_group = group;
			_radius = radiud;
			_mass = mass;
			_center = center;
			_velocity = velocity;
		}

		public int id() {
			return _id;
		}

		public int group() {
			return _group;
		}
		
		public float radius() {
			return _radius;
		}
		
		public float mass() {
			return _mass;
		}
		
		public Vector2f center() {
			return _center;
		}

		public Vector2f velocity() {
			return _velocity;
		}
	}
}
