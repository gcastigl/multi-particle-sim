package ar.com.kiritsu.multiparticlesim.log;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.tp3.logic.AreaStepFunction;

import com.google.common.base.Supplier;

public class LogCollitionTime implements AreaStepFunction {

	private Supplier<Float> _collitionTime;
	private Writer _output;

	public LogCollitionTime(Writer output, Supplier<Float> collitionTime) {
		_output = output;
		_collitionTime = collitionTime;
	}

	@Override
	public Area apply(Area input) {
		BigDecimal collitionTime = new BigDecimal(_collitionTime.get()).setScale(3, RoundingMode.DOWN);
		try {
			_output.write(collitionTime.toString() + "\n");
		} catch (IOException e) {
			throw new IllegalStateException();
		}
		return input;
	}

}
