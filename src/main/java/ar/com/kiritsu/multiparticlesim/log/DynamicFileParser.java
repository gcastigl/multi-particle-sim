package ar.com.kiritsu.multiparticlesim.log;

import java.util.Scanner;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep.ParticleStatus;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public class DynamicFileParser {

	private final String _separator;

	public DynamicFileParser(String separator) {
		_separator = separator;
	}

	public Supplier<DynamicFileStep> parse(final Scanner scanner, int skipFrames) {
		return new DynamicFileReader(scanner, skipFrames);
	}

	private final class DynamicFileReader implements Supplier<DynamicFileStep> {

		private final Scanner _scanner;

		private int index = 0;
		private float _lastStartTime;
		private final int _skipFrames;

		public DynamicFileReader(Scanner scanner, int skipFrames) {
			_scanner = scanner;
			_lastStartTime = 0;
			_skipFrames = skipFrames;
			Preconditions.checkArgument(_skipFrames >= 0);
		}

		@Override
		public DynamicFileStep get() {
			if (!_scanner.hasNext()) {
				return null;
			}
			String[] nextLine = _scanner.nextLine().trim().split("\t");
			float endTime = Float.valueOf(nextLine[0].trim());
			int particlesCount = Integer.valueOf(nextLine[1].trim());
			DynamicFileStep step = new DynamicFileStep(_lastStartTime, endTime - _lastStartTime, index++);
			for (int i = 0; i < particlesCount; i++) {
				String line = _scanner.nextLine().trim();
				String[] columns = line.split(_separator);
				int id = Integer.valueOf(columns[0]);
				int group = Integer.valueOf(columns[1]);
				float radius = Float.valueOf(columns[2]);
				float mass = Float.valueOf(columns[3]);
				Vector2f center = new Vector2f(Float.valueOf(columns[4]), Float.valueOf(columns[5]));
				Vector2f velocity = new Vector2f(Float.valueOf(columns[6]), Float.valueOf(columns[7]));
				step.particles().add(new ParticleStatus(id, group, radius, mass, center, velocity));
			}
			if (_skipFrames > 0) {
				// Skip "_skipFrames" frames (if possible)
				for (int i = 0; i < _skipFrames; i++) {
					if (!_scanner.hasNextLine()) {
						return null;
					}
					nextLine = _scanner.nextLine().trim().split("\t");
					particlesCount = Integer.valueOf(nextLine[1].trim());
					for (int j = 0; j < particlesCount; j++) {
						_scanner.nextLine();
					}
					index++;
				}
			}
			_lastStartTime = endTime;
			return step;
		}

	}
}
