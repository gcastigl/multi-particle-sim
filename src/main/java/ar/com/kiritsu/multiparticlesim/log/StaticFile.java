package ar.com.kiritsu.multiparticlesim.log;

import java.util.List;

import com.google.common.collect.Lists;

public class StaticFile {

	private float _areaSize;
	private List<ParticleDesc> _particles = Lists.newLinkedList();

	public StaticFile(float areaSize) {
		_areaSize = areaSize;
	}

	public float areaSize() {
		return _areaSize;
	}

	public List<ParticleDesc> particles() {
		return _particles;
	}

	public int particlesSize() {
		return _particles.size();
	}

	public static final class ParticleDesc {
		int _id;
		int _color;
		float _radius;

		public ParticleDesc(int id, float radius, int color) {
			_id = id;
			_color = color;
			_radius = radius;
		}
		
		public float radius() {
			return _radius;
		}
		
		public int color() {
			return _color;
		}
		
		public int id() {
			return _id;
		}
	}
}
