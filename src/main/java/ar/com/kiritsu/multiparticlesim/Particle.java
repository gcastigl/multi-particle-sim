package ar.com.kiritsu.multiparticlesim;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.tp4.Movement;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class Particle {

	private final Integer _id;
	private Function<? super Particle, Float> _radius;
	private float _mass;
	private final Movement _movementStatus;
	private final Vector2f _appliedForce = new Vector2f();
	private int _group;
	// TODO: unificar estas dos variables en una sola
	private float expectedVelocity;
	private Vector2f _desiredVelocity = new Vector2f();
	
	private boolean _enabled;

	private List<Vector2f> _previousSteps = Lists.newArrayList();

	public Particle(int id, float radius) {
		this(id, radius, 0, 2);
	}

	public Particle(int id, float radius, float mass, int orders) {
		_id = id;
		_radius = Functions.constant(radius);
		_movementStatus = new Movement(orders);
		_mass = mass;
		_enabled = true;
	}

	public int id() {
		return _id;
	}

	public Particle setEnabled(boolean enabled) {
		_enabled = enabled;
		return this;
	}

	public boolean isEnabled() {
		return _enabled;
	}

	public Movement movementStatus() {
		return _movementStatus;
	}

	public Vector2f position() {
		return _movementStatus.position();
	}

	public Particle setPosition(Vector2f position) {
		position().set(position);
		return this;
	}

	public Vector2f velocity() {
		return _movementStatus.velocity();
	}

	public Vector2f acceleration() {
		return _movementStatus.acceleration();
	}

	public void normalizeVelocity() {
		velocity().normalise().scale(expectedVelocity());
	}

	public Particle setVelocity(Vector2f velocity) {
		velocity().set(velocity);
		return this;
	}

	public float radius() {
		return _radius.apply(this);
	}

	public Particle setRadius(Function<? super Particle, Float> radius) {
		_radius = radius;
		return this;
	}

	public List<Vector2f> steps() {
		return _previousSteps;
	}

	public Particle setSteps(List<Vector2f> _steps) {
		_previousSteps = _steps;
		return this;
	}

	public Particle addStep(Vector2f position) {
		_previousSteps.add(position);
		return this;
	}

	public Particle setExpectedVelocity(float expectedVelocity) {
		this.expectedVelocity = expectedVelocity;
		return this;
	}

	public float expectedVelocity() {
		return expectedVelocity;
	}

	public Particle setDesiredVelocity(Vector2f desiredVelocity) {
		_desiredVelocity.set(desiredVelocity);
		return this;
	}

	public Vector2f desiredVelocity() {
		return _desiredVelocity;
	}

	public Particle setGroup(int group) {
		_group = group;
		return this;
	}

	public int group() {
		return _group;
	}
	
	public Vector2f appliedForce() {
		return _appliedForce;
	}
	
	public Particle setAppliedForce(Vector2f appliedForce) {
		_appliedForce.set(appliedForce);
		return this;
	}

	public float mass() {
		return _mass;
	}
	
	public Particle setMass(float mass) {
		Preconditions.checkArgument(mass > 0);
		_mass = mass;
		return this;
	}

	@Deprecated
	public Particle copy() {
		return new Particle(id(), radius()).setExpectedVelocity(expectedVelocity()).setSteps(steps()).setGroup(group());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
			.append("Id", id())
			.append("radius", radius())
			.append("mass", mass())
			.append("appliedForce", appliedForce())
			.append("position", position())
			.append("velocity", velocity())
			.append("(all derivates)", movementStatus())
			.toString();
	}

	@Override
	public int hashCode() {
		return _id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Particle)) {
			return false;
		}
		Particle other = (Particle) obj;
		return id() == other.id();
	}
}
