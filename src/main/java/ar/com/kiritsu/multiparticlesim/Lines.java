package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

public class Lines {

	public static final Vector2f ponintAt(Line line, float p, Vector2f result) {
		result.set(line.getStart());
		result.x += line.getDX() * p;
		result.y += line.getDY() * p;
		return result;
	}
}
