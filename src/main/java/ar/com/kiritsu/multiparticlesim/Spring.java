package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.geom.Vector2f;

public class Spring {

	private final float _gamma;
	private final float _k;
	private final SpringPoint _origin;
	private final SpringPoint _destination;

	public Spring(float k, SpringPoint origin, SpringPoint destination) {
		this(k, 0, origin, destination);
	}

	public Spring(float k, float gamma, SpringPoint origin, SpringPoint destination) {
		_origin = origin;
		_destination = destination;
		_k = k;
		_gamma = gamma;
	}

	public SpringPoint destination() {
		return _destination;
	}

	public SpringPoint origin() {
		return _origin;
	}

	public float k() {
		return _k;
	}

	public float gamma() {
		return _gamma;
	}

	public float steching() {
		return origin().position().distance(destination().position());
	}

	public static interface SpringPoint {

		Vector2f position();

		void applyForce(Vector2f force);

		Vector2f velocity();

		float mass();

	}

	public static class FixedSpringPoint implements SpringPoint {
		private Vector2f _position;

		public FixedSpringPoint(Vector2f position) {
			_position = position;
		}

		@Override
		public Vector2f position() {
			return _position;
		}

		@Override
		public void applyForce(Vector2f force) {
		}

		@Override
		public Vector2f velocity() {
			return Vectors2f.ZERO();
		}

		@Override
		public float mass() {
			throw new IllegalStateException();
		}
	}

	public static class ParticleSpringPoint implements SpringPoint {
		private Particle _particle;

		public ParticleSpringPoint(Particle particle) {
			_particle = particle;
		}

		@Override
		public Vector2f position() {
			return _particle.position();
		}

		@Override
		public void applyForce(Vector2f force) {
			_particle.appliedForce().add(force);
		}

		@Override
		public Vector2f velocity() {
			return _particle.velocity();
		}

		@Override
		public float mass() {
			return _particle.mass();
		}
		
		public Particle particle() {
			return _particle;
		}
	}
}
