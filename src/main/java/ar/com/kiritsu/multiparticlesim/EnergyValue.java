package ar.com.kiritsu.multiparticlesim;


public class EnergyValue {

	private float _cinematic;
	private float _potential;

	public EnergyValue(float cinematic, float potential) {
		_cinematic = cinematic;
		_potential = potential;
	}

	public float cinematic() {
		return _cinematic;
	}

	public EnergyValue addCinematic(float delta) {
		_cinematic += delta;
		return this;
	}

	public float potential() {
		return _potential;
	}

	public float total() {
		return potential() + cinematic();
	}

	public void add(EnergyValue other) {
		_cinematic += other.cinematic();
		_potential += other.potential();
	}
	
}
