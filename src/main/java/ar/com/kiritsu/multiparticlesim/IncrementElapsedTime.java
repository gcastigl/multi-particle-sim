package ar.com.kiritsu.multiparticlesim;

import ar.com.kiritsu.multiparticlesim.tp3.logic.AreaStepFunction;

import com.google.common.base.Supplier;

public class IncrementElapsedTime implements AreaStepFunction {

	private Supplier<Float> _elapsedTime;

	public IncrementElapsedTime(Supplier<Float> elapsedTime) {
		_elapsedTime = elapsedTime;
	}

	@Override
	public Area apply(Area input) {
		return input.incElapsedTime(_elapsedTime.get());
	}

}
