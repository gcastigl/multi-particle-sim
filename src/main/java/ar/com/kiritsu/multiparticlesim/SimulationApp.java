package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.math.MathUtil;
import ar.com.kiritsu.multiparticlesim.tp3.Simulation;

public abstract class SimulationApp extends BasicGame {

	private Simulation<Area> _simulation;
	private AreaRender _renderer;

	public SimulationApp(String title) {
		super(title);
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		container.setTargetFrameRate(60);
		_simulation = new Simulation<Area>();
		if (_renderer == null) {
			_renderer = new AreaRender();
		}
	}

	public Simulation<Area> simulation() {
		return _simulation;
	}

	public AreaRender renderer() {
		return _renderer;
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		_renderer.render(container, g, simulation().status());
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		if (!_simulation.isFinished() && !container.isPaused()) {
			_simulation.step();
		}
		if (isPressed(Input.KEY_ESCAPE, container)) {
			container.exit();
		}
		if (isPressed(Input.KEY_P, container)) {
			container.setPaused(!container.isPaused());
		}
		if (isPressed(Input.KEY_R, container)) {
			container.reinit();
		}
		if (isDown(Input.KEY_ADD, container) || isDown(Input.KEY_1, container)) {
			setScale(renderer().scale() * 0.95f);
		}
		if (isDown(Input.KEY_MINUS, container) || isDown(Input.KEY_SUBTRACT, container)  || isDown(Input.KEY_2, container)) {
			setScale(renderer().scale() * 1.05f);
		}
		final float deltaOffset = 10f;
		if (isDown(Input.KEY_LEFT, container)) {
			renderer().offset().add(new Vector2f(deltaOffset, 0));
		}
		if (isDown(Input.KEY_RIGHT, container)) {
			renderer().offset().add(new Vector2f(-deltaOffset, 0));
		}
		if (isDown(Input.KEY_UP, container)) {
			renderer().offset().add(new Vector2f(0, deltaOffset));
		}
		if (isDown(Input.KEY_DOWN, container)) {
			renderer().offset().add(new Vector2f(0, -deltaOffset));
		}
	}
	
	protected void setScale(float scale) {
		renderer().setScale(MathUtil.capped(scale, 0, 100));
	}

	private boolean isPressed(int keyCode, GameContainer container) {
		return container.getInput().isKeyPressed(keyCode);
	}

	private boolean isDown(int keyCode, GameContainer container) {
		return container.getInput().isKeyDown(keyCode);
	}
}
