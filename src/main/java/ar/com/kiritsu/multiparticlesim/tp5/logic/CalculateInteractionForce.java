package ar.com.kiritsu.multiparticlesim.tp5.logic;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Vectors2f;
import ar.com.kiritsu.multiparticlesim.Wall;

public class CalculateInteractionForce extends Block<Area> {

	private float _g;
	private final float _kn;
	private final float _kt;
	private float _gamma;

	public CalculateInteractionForce(float kn, float kt, float g, float gamma) {
		_kn = kn;
		_kt = kt;
		_g = g;
		_gamma = gamma;
	}

	@Override
	protected void exec(Area input) {
		FindNeighbors findNeighbors = new FindNeighbors(5).at(input);
		for (Particle particle : input.particles()) {
			particle.appliedForce().set(Vectors2f.ZERO());
			float r = particle.radius();
			for (Particle neighbor : findNeighbors.centeredOn(particle).find()) {
				Vector2f rij = neighbor.position().copy().sub(particle.position());
				float overlap = particle.radius() + neighbor.radius() - rij.length();
				if (overlap > 0) {
					Vector2f en = rij.copy().normalise();
					Vector2f et = new Vector2f(-en.y, en.x);
					Vector2f fn = fn(overlap, en, particle.velocity(), _gamma);
					Vector2f ft = ft(particle, neighbor.velocity(), overlap, et);
					particle.appliedForce().add(fn).add(ft);
				}
			}
			Vector2f closesPoint = new Vector2f();
			for (Wall wall : input.obstacles()) {
				Vector2f position = particle.position();
				wall.bounds().getClosestPoint(position, closesPoint);
				float lineDist = position.distance(closesPoint);
				if (lineDist < r) {
					float overlap = r - lineDist;
					Vector2f en = closesPoint.copy().sub(position).normalise();
					Vector2f et = new Vector2f(-en.y, en.x);
					Vector2f fn = fn(overlap, en, particle.velocity(), 4);
					Vector2f ft = ft(particle, Vectors2f.ZERO(), overlap, et).scale(1/30f);
					particle.appliedForce().add(fn).add(ft);
				}
			}
			particle.appliedForce().y += particle.mass() * _g;
		}
	}

	private Vector2f fnTmp = new Vector2f();
	private Vector2f fn(float overlap, Vector2f en, Vector2f velocity, float gamma) {
		fnTmp.set(velocity);
		return new Vector2f(en).scale(-_kn * overlap).sub(fnTmp.scale(gamma));
	}

	private Vector2f ft(Particle p1, Vector2f v2, float overlap, Vector2f et) {
		Vector2f vrel = p1.velocity().copy().sub(v2);
		float ft = -_kt * overlap * vrel.dot(et);
		return new Vector2f(et).scale(ft);
	}
}
