package ar.com.kiritsu.multiparticlesim.tp5;

import java.io.File;
import java.io.IOException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.SimulationApp;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.log.AreaFilesHandler;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep.ParticleStatus;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class GUIGranularAreaApp extends SimulationApp {

	public static void main(String[] args) throws Exception {
		GranularAreaApp.main(args);
		AppGameContainer appContainer = new AppGameContainer(new GUIGranularAreaApp());
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(700, 700, false);
		appContainer.start();
	}

	private final int _skipFrames = 6;
	private AreaFilesHandler _areaFileHandler;
	private boolean _finished;

	public GUIGranularAreaApp() {
		super("TP 5");
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		super.init(container);
		try {
			File staticFile = new File("src/main/resources/tp5/static.txt");
			File dynamicFile = new File("src/main/resources/tp5/dynamic.txt");
			File areaFile = new File("src/main/resources/tp5/area.txt");
			_areaFileHandler = new AreaFilesHandler(staticFile, dynamicFile, areaFile);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		renderer().setScale(120)
			.andDebugVelocity(.5f)
			.setParticleColor(new Function<Particle, Color>() {
				@Override
				public Color apply(Particle input) {
					return input.radius() == 0.1 ? Color.green : Color.red;
				}
			});
		simulation().setInitialState(new Area()).onStart(new Block<Area>() {
			@Override
			protected void exec(Area input) {
				initialize(input);
			}
		}).onStep(new Block<Area>() {
			@Override
			protected void exec(Area input) {
				nextStep(input);
			}
		})
		.setCutCondition(new Predicate<Area>() {
			@Override
			public boolean apply(Area input) {
				return _finished;
			}
		})
		;
	}

	private void initialize(Area input) {
		Vector2f bounds = new Vector2f(_areaFileHandler.areaSize(), _areaFileHandler.areaSize());
		for (Line wallDef : _areaFileHandler.getWallsDef()) {
			input.addObstale(new Wall().setBounds(wallDef));
		}
		input.setBounds(bounds);
		int id = 0;
		for (ParticleStatus status : _areaFileHandler.nextSteps().particles()) {
			Preconditions.checkState(id == status.id());
			input.particles().add(
				new Particle(status.id(), status.radius())
					.setPosition(status.center())
					.setVelocity(status.velocity())
					.setMass(status.mass())
					.setRadius(Functions.constant(status.radius()))
			);
			id++;
		}
		input.incElapsedTime(0);
	}

	private void nextStep(Area input) {
		DynamicFileStep currentStep = _areaFileHandler.nextSteps();
		for (int i = 0; i < _skipFrames; i++) {
			currentStep = _areaFileHandler.nextSteps();
		}
		if (currentStep == null) {
			_finished = true;
			return;
		}
		float energy = 0;
		for (ParticleStatus status : currentStep.particles()) {
			Particle particle;
			try {
				particle = Iterables.get(input.particles(), status.id());
			} catch (IndexOutOfBoundsException e) {
				// Create this MF
				particle = new Particle(status.id(), status.radius(), status.mass(), 5);
				input.particles().add(particle);
			}
			Preconditions.checkState(particle.id() == status.id());
			particle
				.setPosition(status.center())
				.setVelocity(status.velocity());
			float v = particle.velocity().length();
			float h = 5 - particle.position().y;
			energy += particle.mass() * (v * v / 2 + 9.8 * h);
		}
		input.incElapsedTime(currentStep.duration());
//		System.out.println(energy);
	}
}
