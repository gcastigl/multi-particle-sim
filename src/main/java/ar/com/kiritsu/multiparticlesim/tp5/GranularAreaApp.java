package ar.com.kiritsu.multiparticlesim.tp5;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.ElapsedTimeIsGreaterThan;
import ar.com.kiritsu.multiparticlesim.IncrementElapsedTime;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.log.LogAreaToFile;
import ar.com.kiritsu.multiparticlesim.tp3.Simulation;
import ar.com.kiritsu.multiparticlesim.tp4.logic.GearPredictorStep;
import ar.com.kiritsu.multiparticlesim.tp5.logic.CalculateInteractionForce;
import ar.com.kiritsu.multiparticlesim.tp5.logic.DropParticles;

import com.google.common.base.Suppliers;
import com.google.common.collect.Lists;

public class GranularAreaApp {

	public static void main(String[] args) throws SlickException {
		new GranularAreaApp("./src/main/resources/tp5/").run();
	}

	private Simulation<Area> _simulation;
	private FileWriter _dynamicWriter;
	private FileWriter _areaWriter;

	private final String _dir;
	
	private List<Wall>  _doors = Lists.newLinkedList();
	
	public GranularAreaApp(String dir) {
		_dir = dir;
	}
	
	public void run() {
		final float L = 4;
		final float W = 2f;
		final float D = 1;
		final float r = (D / 10) / 2f;
		final float m = 1f;
		final float kn = (float) 10E3;
		final float kt = 2 * kn;
//		final float dt = (float) (0.1f * Math.sqrt(0.1f / kn));
		final float dt = 1 / 10000f;
		float maxTime = 15f;
		setupWriters();
		_simulation = new Simulation<>();
		final float LTop = L * 2 / 5f;
		final float LBot = LTop + L / 3;
		_simulation
			.setInitialState(new Area().setBounds(new Vector2f(W * 4, L * 4)))
			.onStart(new Block<Area>() {
				@Override
				protected void exec(Area input) {
					// Silo entero
					input.addObstale(new Wall(new Line(new Vector2f(0, 0), new Vector2f(0, L))));
					input.addObstale(new Wall(new Line(new Vector2f(0, L), new Vector2f(W, L))));
					input.addObstale(new Wall(new Line(new Vector2f(W, L), new Vector2f(W, 0))));
					// Diagonales
					// Izquierda
					input.addObstale(new Wall(new Line(new Vector2f(0, LTop), new Vector2f(W * 2 / 5f, LBot))));
					// Derecha
					input.addObstale(new Wall(new Line(new Vector2f(W, LTop), new Vector2f(W * 3 / 5f, LBot))));

					// Triple wall to avoid objects from trespassing
					input.addObstale(new Wall(new Line(new Vector2f(0, L + .01f), new Vector2f(W, L +.01f))));
					input.addObstale(new Wall(new Line(new Vector2f(0, L + .02f), new Vector2f(W, L +.02f))));
					input.addObstale(new Wall(new Line(new Vector2f(0, L + .035f), new Vector2f(W, L +.035f))));
					
					Wall door1, door2, door3;
					input.addObstale(door1 = new Wall(new Line(new Vector2f(0, LBot + .01f), new Vector2f(W, LBot +.01f))));
					input.addObstale(door2 = new Wall(new Line(new Vector2f(0, LBot + .02f), new Vector2f(W, LBot +.02f))));
					input.addObstale(door3 = new Wall(new Line(new Vector2f(0, LBot + .03f), new Vector2f(W, LBot +.03f))));
					_doors.add(door1);
					_doors.add(door2);
					_doors.add(door3);
					
				}
			})
			.onStep(new LogAreaToFile(_dynamicWriter, _areaWriter, 4))
			.onStep(new DropParticles(r, m, LTop - 0.1f, W, 120, 0.2f))
			.onStep(new CalculateInteractionForce(kn, kt, 9.8f, 40))
			.onStep(new GearPredictorStep(dt))
			.onStep(new Block<Area>() {
				private boolean _doorsRemoved = false;
				@Override
				protected void exec(Area input) {
					if (input.elapsedTime() > 2f && !_doorsRemoved) {
						input.obstacles().removeAll(_doors);
						_doorsRemoved = true;
					}
				}
			})
			.onStep(new IncrementElapsedTime(Suppliers.ofInstance(dt)))
			.onEnd(new Block<Area>() {
				@Override
				protected void exec(Area input) {
					closeWriters();
				}
			})
			.setCutCondition(new ElapsedTimeIsGreaterThan(maxTime));
		int lastP = 0;
		do {
			_simulation.step();
			int p = (int) ((_simulation.status().elapsedTime() / maxTime) * 100);
			if (lastP != p && p % 10 == 0) {
				lastP = p;
				System.out.println(p + "% compleated...");
			}
		} while(!_simulation.isFinished());
	}
	
	private void setupWriters() {
		try {
			_dynamicWriter = new FileWriter(_dir + "dynamic.txt");
			_areaWriter = new FileWriter(_dir + "area.txt");		
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	private void closeWriters() {
		try {
			_dynamicWriter.close();
			_areaWriter.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

}
