package ar.com.kiritsu.multiparticlesim.tp5.logic;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

public class DropParticles extends Block<Area> {

	private float _r, _m;
	private int _droppedParticles = 0;
	private int _maxParticles = 0;
	private float _lastTimeDrop = 0;
	private final float _width;
	private float _h;
	private float _tbdd;

	public DropParticles(float r, float m, float h, float width, int maxParticles, float tbdd) {
		_r = r;
		_m = m;
		_width = width;
		_maxParticles = maxParticles;
		_tbdd = tbdd;
		_h = h;
	}

	@Override
	protected void exec(Area input) {
		if (input.elapsedTime() - _lastTimeDrop > _tbdd && _droppedParticles < _maxParticles) {
			_lastTimeDrop = input.elapsedTime();
			for (float x = _r; x < _width - _r; x += 3 * _r) {
				input.particles().add(new Particle(input.particles().size(), _r, _m, 5).setPosition(new Vector2f(x, _h)));
				_droppedParticles++;
			}
		}
	}

}
