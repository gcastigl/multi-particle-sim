package ar.com.kiritsu.multiparticlesim.verlet;

import java.util.Collection;

import org.apache.log4j.Logger;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Optional;

public class CellBasedNeighbors {

	private static final Logger logger = Logger.getLogger(CellBasedNeighbors.class);
	private static final int[][] NEIGHBORS = { { -1, 1 }, { 0, 1 }, { 1, 1 }, { -1, 0 }, { 0, 0 }, { 1, 0 }, { -1, -1 }, { 0, -1 }, { 1, -1 } };

	private Cell[][] _cells;
	private final int _cellsCount;
	private float _cellSize;
	private Optional<Block<Particle>> _outOfBoundsHandler = Optional.absent();

	public CellBasedNeighbors(int cellsCount, float cellSize) {
		_cellsCount = cellsCount;
		_cellSize = cellSize;
		clear();
	}

	public CellBasedNeighbors outOfBoundsHandler(Block<Particle> outOfBoundsHandler) {
		_outOfBoundsHandler = Optional.of(outOfBoundsHandler);
		return this;
	}

	public void clear() {
		_cells = new Cell[_cellsCount][_cellsCount];
		for (Cell[] row : _cells) {
			for (int i = 0; i < _cellsCount; i++) {
				row[i] = new Cell();
			}
		}
	}

	public void addAll(Iterable<Particle> particles) {
		for (Particle particle : particles) {
			add(particle);
		}
	}

	public void add(Particle particle) {
		int cellx = cellIndex(particle.position().x);
		int celly = cellIndex(particle.position().y);
		try {
			_cells[cellx][celly].elements().add(particle);
		} catch (ArrayIndexOutOfBoundsException e) {
			if (_outOfBoundsHandler.isPresent()) {
				_outOfBoundsHandler.get().apply(particle);
			} else {
				System.out.println("Error: " + particle.position());
				logger.error("Particle out of bounds: " + particle.position());
//				throw e;
			}
		}
	}

	public Collection<Particle> neighbors(Particle particle, float rc, Collection<Particle> result) {
		int cellx = cellIndex(particle.position().x);
		int celly = cellIndex(particle.position().y);
		for (int[] neighbor : NEIGHBORS) {
			int nx = cellx + neighbor[0];
			int ny = celly + neighbor[1];
			if (isWithinCellBounds(nx, ny)) {
				for (Particle other : _cells[nx][ny].elements()) {
					if (isWithinInteraction(particle, other, rc)) {
						result.add(other);
					}
				}
			}
		}
		return result;
	}

	private boolean isWithinCellBounds(int x, int y) {
		return 0 <= x && x < _cellsCount && 0 <= y && y < _cellsCount;
	}

	private boolean isWithinInteraction(Particle p1, Particle p2, float rc) {
		if (p1 == p2) {
			return false;
		}
		float minDistance = p1.radius() + rc + p2.radius();
		return p1.position().distanceSquared(p2.position()) <= (minDistance * minDistance);
	}

	private int cellIndex(float value) {
		return (int) (value / _cellSize);
	}

	@Override
	public String toString() {
		String s = "";
		for (Cell[] row : _cells) {
			String rowString = "";
			for (Cell cell : row) {
				rowString += cell.elements().size() + "\t";
			}
			s += rowString + "\n";
		}
		return s;
	}
}
