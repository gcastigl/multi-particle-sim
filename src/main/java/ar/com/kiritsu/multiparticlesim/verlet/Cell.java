package ar.com.kiritsu.multiparticlesim.verlet;

import java.util.List;

import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.collect.Lists;

public class Cell {

	private List<Particle> _elements = Lists.newLinkedList();

	public List<Particle> elements() {
		return _elements;
	}

}
