package ar.com.kiritsu.multiparticlesim.verlet;

import java.io.Serializable;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

public class Particles {

	private static final Function<Particle, Vector2f> POSITION = new Function<Particle, Vector2f>() {
		@Override
		public Vector2f apply(Particle input) {
			return input.position();
		}
	};

	private static final Function<Particle, Vector2f> VELOCITY = new Function<Particle, Vector2f>() {
		@Override
		public Vector2f apply(Particle input) {
			return input.velocity();
		}
	};

	private static final Function<Particle, Float> MASS = new Function<Particle, Float>() {
		@Override
		public Float apply(Particle input) {
			return input.mass();
		}
	};

	private static final Function<Particle, Float> RADIUS = new Function<Particle, Float>() {
		@Override
		public Float apply(Particle input) {
			return input.radius();
		}
	};

	private static final Function<Particle, Integer> GROUP = new Function<Particle, Integer>() {
		@Override
		public Integer apply(Particle input) {
			return input.group();
		}
	};

	public static Predicate<Particle> forId(final int id) {
		return new Predicate<Particle>() {
			@Override
			public boolean apply(Particle input) {
				return input.id() == id;
			}
		};
	}

	private static Predicate<Particle> IS_ENABLED = new Predicate<Particle>() {
		@Override
		public boolean apply(Particle input) {
			return input.isEnabled();
		}
	};

	public static final Predicate<Particle> isEnabled() {
		return IS_ENABLED;
	}

	public static Function<Particle, Serializable> id() {
		return new Function<Particle, Serializable>() {
			@Override
			public Serializable apply(Particle input) {
				return input.id();
			}
		};
	}

	public static Function<Particle, Vector2f> position() {
		return POSITION;
	}

	public static Function<Particle, Vector2f> velocity() {
		return VELOCITY;
	}

	public static Function<Particle, Float> mass() {
		return MASS;
	}
	
	public static Function<Particle, Float> radius() {
		return RADIUS;
	}

	public static Function<Particle, Integer> group() {
		return GROUP;
	}
}
