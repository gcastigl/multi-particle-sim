package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.slick.Renderer;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

public class AreaRender implements Renderer<Area> {

	private Function<? super Particle, Color> _particleColor;
	private Circle _particleShape;
	private Line _arrow = new Line(0, 0);
	private Line _springShape = new Line(0, 0);
	private final Vector2f _offset = new Vector2f();
	private boolean _debugVelocity = false;
	private Optional<Image> _background = Optional.absent();
	private Optional<Renderer<Area>> _postRender = Optional.absent();
	private Function<? super Particle, Optional<Image>> _particleImage;

	private float _scale = 1;
	private float _directorLen = 1;

	public AreaRender() {
		_particleShape = new Circle(0, 0, 0);
		setParticleColor(Functions.constant(Color.red));
		Optional<Image> absent = Optional.absent();
		_particleImage = Functions.constant(absent);
	}

	public AreaRender setBackground(Image background) {
		_background = Optional.of(background);
		return this;
	}

	public AreaRender setPostRender(Renderer<Area> postRender) {
		_postRender = Optional.of(postRender);
		return this;
	}
	
	public AreaRender setParticleColor(Function<? super Particle, Color> color) {
		_particleColor = color;
		return this;
	}

	public AreaRender setParticleImage(Function<? super Particle, Optional<Image>> _particleImage) {
		this._particleImage = _particleImage;
		return this;
	}

	public AreaRender setScale(float scale) {
		Preconditions.checkArgument(scale >= 0);
		this._scale = scale;
		return this;
	}

	public float scale() {
		return _scale;
	}
	
	public Vector2f offset() {
		return _offset;
	}
	
	public void render(GameContainer container, Graphics g, Area area) throws SlickException {
		if (_background.isPresent()) {
			float width = container.getWidth();
			float height = container.getHeight();
			float srcx = 0;
			float srcy = 0;
			g.drawImage(_background.get(), 0, 0, width, height, srcx, srcy, width, height, Color.cyan);
		}
		for (Particle particle : area.particles()) {
			drawParticle(particle, g, container.getWidth(), container.getHeight());
		}
		g.setColor(Color.blue);
		for (Spring spring : area.springs()) {
			_springShape.set(toScreen(spring.origin().position()), toScreen(spring.destination().position()));
			g.draw(_springShape);
		}
		g.setColor(Color.white);
		Vector2f startScaled = new Vector2f();
		Vector2f endScaled = new Vector2f();
		for (Wall wall : area.obstacles()) {
			startScaled = toScreen(wall.bounds().getStart());
			endScaled = toScreen(wall.bounds().getEnd());
			g.drawLine(startScaled.x, startScaled.y, endScaled.x, endScaled.y);
		}
		g.setColor(Color.red);
		if (container.isPaused()) {
			g.drawString("Paused", 10, 30);
		}
		g.drawString("Time: " + area.elapsedTime(), 10, container.getHeight() - 30);
		g.drawString("Particles: " + area.particles().size(), 10, container.getHeight() - 20);
		if (_postRender.isPresent()) {
			_postRender.get().render(container, g, area);
		}
	}
	
	private void drawParticle(Particle particle, Graphics g, int with, int height) {
		Vector2f scaledPosition = toScreen(particle.position());
		float scaledRadius = particle.radius() * _scale;
		if (scaledPosition.x + scaledRadius < 0 || with < scaledPosition.x - scaledRadius) {
			return;
		}
		if (scaledPosition.y + scaledRadius < 0 || height < scaledPosition.y - scaledRadius) {
			return;
		}
		Optional<Image> image = _particleImage.apply(particle);
		if (image.isPresent()) {
			int length = (int) scaledRadius * 2;
			float x = scaledPosition.x - scaledRadius;
			float y = scaledPosition.y - scaledRadius;
			g.drawImage(image.get().getScaledCopy(length, length), x, y);
		} else {
			_particleShape.setRadius(scaledRadius);
			g.setColor(_particleColor.apply(particle));
			_particleShape.setCenterX(scaledPosition.x);
			_particleShape.setCenterY(scaledPosition.y);
			g.fill(_particleShape);			
		}
		if (_debugVelocity) {
			g.setColor(Color.green);
			Vector2f arrowEnd = new Vector2f(particle.velocity());
			float dirLen = Math.min(_directorLen, particle.velocity().length());
			arrowEnd.normalise().scale(particle.radius() + dirLen * _scale).add(scaledPosition);
			_arrow.set(scaledPosition, arrowEnd);
			g.draw(_arrow);
		}
		if (!particle.steps().isEmpty()) {
			g.setColor(Color.white);
			for (Vector2f step : particle.steps()) {
				Vector2f scaledStep = step.copy().scale(_scale).add(_offset);
				g.drawLine(scaledStep.x, scaledStep.y, scaledStep.x, scaledStep.y);
			}
		}
	}

	public AreaRender andDebugVelocity(float directorLen) {
		_debugVelocity = true;
		_directorLen = directorLen;
		return this;
	}

	public Vector2f toScreen(Vector2f v) {
		return v.copy().scale(scale()).add(offset()); 
	}
}
