package ar.com.kiritsu.multiparticlesim;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.tp4.force.ForceEmitter;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Area {

	private float _elapsedTime;
	private final Vector2f _bounds = new Vector2f();
	private final Map<Integer, Particle> _particles = Maps.newHashMap();
	private final List<Wall> _obstacles = Lists.newLinkedList();
	private List<Spring> _springs = Lists.newLinkedList();
	private List<ForceEmitter> _forces = Lists.newArrayList();

	public Collection<Particle> particles() {
		return _particles.values();
	}

	public Collection<Particle> particles(Predicate<Particle> filter) {
		return Collections2.filter(particles(), filter);
	}

	public Optional<Particle> getParticleById(int id) {
		return Optional.fromNullable(_particles.get(id));
	}

	public Optional<Particle> tryFindParticle(Predicate<Particle> predicate) {
		return Iterables.tryFind(particles(), predicate);
	}

	public void removeParticles(Collection<Particle> toRemove) {
		particles().removeAll(toRemove);
	}

	public void removeParticlesIf(Predicate<? super Particle> toRemove) {
		Iterables.removeIf(particles(), toRemove);
	}

	public Area addParticle(Particle particle) {
		_particles.put(particle.id(), particle);
		return this;
	}

	public Area addParticle(Particle... particles) {
		for (Particle particle : particles) {
			addParticle(particle);
		}
		return this;
	}

	public Area addObstale(Wall obstace) {
		obstacles().add(obstace);
		return this;
	}

	public Area setObstacles(List<Wall> obstacles) {
		_obstacles.addAll(obstacles);
		return this;
	}

	public List<Wall> obstacles() {
		return _obstacles;
	}

	public Area setBounds(Vector2f bounds) {
		_bounds.set(bounds);
		return this;
	}

	public Vector2f bounds() {
		return _bounds;
	}

	public Area setElapsedTime(float elapsedTime) {
		_elapsedTime = elapsedTime;
		return this;
	}

	public Area incElapsedTime(float elapsedTime) {
		setElapsedTime(elapsedTime() + elapsedTime);
		return this;
	}

	public float elapsedTime() {
		return _elapsedTime;
	}

	public List<ForceEmitter> forces() {
		return _forces;
	}

	public List<Spring> springs() {
		return _springs;
	}

	@Deprecated
	public Area copy() {
		return new Area().setBounds(bounds()).setElapsedTime(elapsedTime()).setObstacles(obstacles());
	}

}
