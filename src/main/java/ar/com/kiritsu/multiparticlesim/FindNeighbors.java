package ar.com.kiritsu.multiparticlesim;

import java.util.Collection;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.verlet.CellBasedNeighbors;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

public class FindNeighbors {

	private final Collection<Particle> resultCache = Lists.newLinkedList();

	private final int _cellsCount;
	private final Optional<Block<Particle>> _outOfBoundsHandler;
	private CellBasedNeighbors _strategy;
	private Particle _center;
	private float _r;

	public FindNeighbors(int cellsCount) {
		this(cellsCount, null);
	}

	public FindNeighbors(int cellsCount, Block<Particle> outOfBoundsHandler) {
		_cellsCount = cellsCount;
		_outOfBoundsHandler = Optional.fromNullable(outOfBoundsHandler);
	}

	public FindNeighbors at(Area area) {
		_strategy = new CellBasedNeighbors(_cellsCount, area.bounds().x / _cellsCount);
		if (_outOfBoundsHandler.isPresent()) {
			_strategy.outOfBoundsHandler(_outOfBoundsHandler.get());
		}
		_strategy.addAll(area.particles());
		return this;
	}

	public FindNeighbors centeredOn(Particle center) {
		_center = center;
		return this;
	}

	public FindNeighbors atDistance(float r) {
		_r = r;
		return this;
	}

	public Collection<Particle> find() {
		resultCache.clear();
		_strategy.neighbors(_center, _r, resultCache);
		return resultCache;
	}

}
