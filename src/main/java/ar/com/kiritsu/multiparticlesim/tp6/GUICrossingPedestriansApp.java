package ar.com.kiritsu.multiparticlesim.tp6;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.SimulationApp;
import ar.com.kiritsu.multiparticlesim.log.AreaFilesHandler;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep.ParticleStatus;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

public class GUICrossingPedestriansApp extends SimulationApp {

	private static final String DIR = "./src/main/resources/tp6/";
	
	public static void main(String[] args) throws Exception {
		new CrossingPedestriansApp(DIR).run();
		AppGameContainer appContainer = new AppGameContainer(new GUICrossingPedestriansApp());
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(700, 700, false);
		appContainer.start();
	}

	private final int _skipFrames = 5;
	private AreaFilesHandler _areaFileHandler;
	private boolean _finished;

	public GUICrossingPedestriansApp() {
		super("Cruzados");
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		super.init(container);
		try {
			File dynamicFile = new File(DIR + "dynamic.txt");
			File areaFile = new File(DIR + "area.txt");
			_areaFileHandler = new AreaFilesHandler(dynamicFile, areaFile, _skipFrames);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		_finished = false;
		renderer().offset().set(new Vector2f(50, 0));
		renderer()
			.setScale(5)
//			.andDebugVelocity(.5f)
			.setParticleColor(new Function<Particle, Color>() {
				@Override
				public Color apply(Particle input) {
					return input.group() == 1 ? Color.yellow : Color.blue;
				}
			});
		simulation()
			.setInitialState(initialize(new Area()))
			.onStep(new Block<Area>() {
				@Override
				protected void exec(Area input) {
					nextStep(input);
				}
			}).setCutCondition(new Predicate<Area>() {
				@Override
				public boolean apply(Area input) {
					return _finished;
				}
			});
	}

	private Area initialize(Area input) {
		return input;
	}

	private void nextStep(Area input) {
		DynamicFileStep currentStep = _areaFileHandler.nextSteps();
		if (currentStep == null) {
			_finished = true;
			input.removeParticlesIf(Predicates.alwaysTrue());
			return;
		}
		removeDisabledParticles(input, currentStep);
		for (ParticleStatus status : currentStep.particles()) {
			Optional<Particle> particle = input.getParticleById(status.id());
			if (!particle.isPresent()) {
				Particle newParticle = new Particle(status.id(), status.radius(), status.mass(), 5);
				particle = Optional.of(newParticle);
				input.addParticle(newParticle);
			}
			particle.get().setPosition(status.center()).setVelocity(status.velocity());
			particle.get().setGroup(status.group());
		}
		input.incElapsedTime(currentStep.duration());
	}

	private void removeDisabledParticles(Area input, DynamicFileStep currentStep) {
		final Set<Integer> ids = new HashSet<>(currentStep.particles().size());
		for (ParticleStatus status : currentStep.particles()) {
			ids.add(status.id());
		}
		input.removeParticlesIf(new Predicate<Particle>() {
			@Override
			public boolean apply(Particle input) {
				return !ids.contains(input.id());
			}
		});
	}
}
