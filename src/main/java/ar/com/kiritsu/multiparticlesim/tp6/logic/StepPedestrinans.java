package ar.com.kiritsu.multiparticlesim.tp6.logic;

import static ar.com.kiritsu.multiparticlesim.verlet.Particles.isEnabled;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;

import java.util.Collection;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Vectors2f;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

public class StepPedestrinans extends Block<Area> {

	private static final float TAU = 0.5f;
	private static final float ALPHA = 800f, BETA = 1.5f;
	private static final float KN = 10000;
	private static final float P = 0.5f;

	private final Vector2f _tmp = new Vector2f();
	private final int _cellsCount;

	public StepPedestrinans(int cellsCount) {
		_cellsCount = cellsCount;
	}

	@Override
	protected void exec(Area input) {
		FindNeighbors findNighbors = new FindNeighbors(_cellsCount).at(input);
		for (Particle particle : input.particles()) {
			particle.appliedForce().set(Vectors2f.ZERO());
			findNighbors.centeredOn(particle);
			fG(particle, findNighbors.find(), _tmp);
			fDesire(particle, _tmp);
		}
		fSocial(input);
		final Predicate<Particle> outOfBounds = new OutOfBounds(input);
		final Predicate<Particle> toRemove = or(not(isEnabled()), outOfBounds);
		input.removeParticlesIf(toRemove);
	}

	private void fG(Particle p, Collection<Particle> collitions, Vector2f tmp) {
//		if (collitions.size() != 0) {
//			System.out.println(collitions.size());
//		}
		for (Particle other : collitions) {
			if (other.group() != p.group()) {
				boolean disabled = Math.random() < P;
				p.setEnabled(!disabled);
				other.setEnabled(disabled);
			}
			float eps = (p.radius() + other.radius()) - p.position().distance(other.position());
			Preconditions.checkArgument(eps >= 0);
			Vectors2f.versor(p.position(), other.position(), tmp);
			Vector2f fn = tmp;
			fn.scale(-KN * eps);
			p.appliedForce().add(fn);
		}
	}

	private final Vector2f _e = new Vector2f();
	private void fSocial(Area input) {
		Collection<Particle> particles = Lists.newArrayList(input.particles());
		for (Particle p : particles) {
			for (Particle other : particles) {
				if (other == p) {
					// skip self
					continue;
				}
				if (other.id() < p.id()) {
					continue;
				}
//				if (other.group() == p.group()) {
//					continue;
//				}
				_e.set(0, 0);
				float dist = p.position().distance(other.position());
				float c = (float) Math.pow(Math.E, -dist / BETA);
				Vectors2f.versor(other.position(), p.position(), _e);
				// e is now the director e_ij
				_e.scale(c * ALPHA);
				p.appliedForce().add(_e);
				other.appliedForce().add(_e.scale(-1));
			}
		}
	}

	private void fDesire(Particle p, Vector2f tmp) {
		tmp.set(Vectors2f.ZERO());
		tmp.add(p.desiredVelocity()).sub(p.velocity());
		tmp.scale(p.mass() / TAU);
		p.appliedForce().add(tmp);
	}

	private static class OutOfBounds implements Predicate<Particle> {

		private final Area _area;

		public OutOfBounds(Area area) {
			_area = area;
		}

		@Override
		public boolean apply(Particle input) {
			Vector2f bounds = _area.bounds();
			if (input.position().x < 0 || input.position().y < 0) {
				return true;
			} else if (input.position().x >= bounds.x || input.position().y > bounds.y) {
				return true;
			}
			return false;
		}

	}
}
