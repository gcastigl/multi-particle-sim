package ar.com.kiritsu.multiparticlesim.tp6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.ElapsedTimeIsGreaterThan;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.log.LogAreaToFile;
import ar.com.kiritsu.multiparticlesim.tp3.Simulation;
import ar.com.kiritsu.multiparticlesim.tp4.logic.GearPredictorStep;
import ar.com.kiritsu.multiparticlesim.tp6.logic.StepPedestrinans;

public class CrossingPedestriansApp implements Runnable {

	public static void main(String[] args) throws IOException {
		new CrossingPedestriansApp("./src/main/resources/tp6/").run();
	}

	private Simulation<Area> _simulation;
	private FileWriter _dynamicWriter;
	private FileWriter _areaWriter;

	private final float _dt = 1 / 1000f;

	public CrossingPedestriansApp(String dir) throws IOException {
		_dynamicWriter = new FileWriter(new File(dir + "dynamic.txt"));
		_areaWriter = new FileWriter(new File(dir + "area.txt"));
	}

	@Override
	public void run() {
		final float T = 30;
		final int CELLS = 100;
		_simulation = new Simulation<Area>()
			.setInitialState(new Area().setBounds(new Vector2f(1000, 1000)))
			.onStart(new Block<Area>() {
				@Override
				protected void exec(Area input) {
					setup(input);
				}
			})
			.onStep(new LogAreaToFile(_dynamicWriter, _areaWriter, 2))
			.onStep(new StepPedestrinans(CELLS))
			.onStep(new GearPredictorStep(_dt))
			.onStep(new Block<Area>() {
				private int _lastP = -1;
				@Override
				protected void exec(Area input) {
					input.incElapsedTime(_dt);
					int p = (int) ((input.elapsedTime() / T) * 100);
					if (p % 5 == 0 && p != _lastP) {
						_lastP = p;
						System.out.println(p + "%");
					}
				}
			})
			.onEnd(new Block<Area>() {
				@Override
				protected void exec(Area input) {
					try {
						_dynamicWriter.close();
						_areaWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			})
			.setCutCondition(new ElapsedTimeIsGreaterThan(T));
		;
		do {
			_simulation.step();
		} while (!_simulation.isFinished());
	}

	private void setup(Area input) {
		final float g0xStart = 10;
		final float g1xStart = 55;
		final float g0yStart = 10.5f;
		final float g1yStart = 10;
		final float r = 1;
		int pid = 0;
		Vector2f G0V = new Vector2f(1.6f, 0);
		Vector2f G1V = new Vector2f(-1.6f, 0);
		for (int row = 0; row < 20; row++) {
			for (int col = 0; col < 15; col++) {
				Particle g0 = new Particle(pid++, r, 60, 5)
					.setPosition(new Vector2f(g0xStart + col * r * 3f, row * r * 3f + g0yStart))
					.setDesiredVelocity(G0V)
					.setGroup(0);
				Particle g1 = new Particle(pid++, r, 60, 5)
					.setPosition(new Vector2f(g1xStart + col * r * 3f, row * r * 3f + g1yStart))
					.setDesiredVelocity(G1V)
					.setGroup(1); 
				input.addParticle(g0, g1);
			}
		}
	}

}
