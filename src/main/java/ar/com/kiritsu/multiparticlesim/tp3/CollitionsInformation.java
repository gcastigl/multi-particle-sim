package ar.com.kiritsu.multiparticlesim.tp3;

import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class CollitionsInformation {

	private float _minCollitionTime;
	private List<CollitionInformation> _collitions = Lists.newLinkedList();

	public void clear() {
		_collitions.clear();
		_minCollitionTime = Float.POSITIVE_INFINITY;
	}

	public CollitionsInformation clearIf(boolean condition) {
		if (condition) {
			clear();
		}
		return this;
	}

	public CollitionsInformation addCollition(CollitionInformation collition) {
		_collitions.add(collition);
		return this;
	}

	public CollitionsInformation setMinCollitionTime(float minCollitionTime) {
		_minCollitionTime = minCollitionTime;
		return this;
	}

	public boolean updateMinCollitionTime(float minCollitionTime) {
		if (Float.isInfinite(minCollitionTime)) {
			return false;
		}
		Preconditions.checkArgument(minCollitionTime >= 0);
		if (minCollitionTime <= _minCollitionTime) {
			clearIf(minCollitionTime < _minCollitionTime);
			_minCollitionTime = minCollitionTime;
			return true;
		}
		return false;
	}

	public float minCollitionTime() {
		return _minCollitionTime;
	}

	public List<CollitionInformation> collitions() {
		return _collitions;
	}
}
