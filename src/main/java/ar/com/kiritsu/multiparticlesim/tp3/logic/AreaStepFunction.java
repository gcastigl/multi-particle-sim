package ar.com.kiritsu.multiparticlesim.tp3.logic;

import ar.com.kiritsu.multiparticlesim.Area;

import com.google.common.base.Function;

public interface AreaStepFunction extends Function<Area, Area> {

}
