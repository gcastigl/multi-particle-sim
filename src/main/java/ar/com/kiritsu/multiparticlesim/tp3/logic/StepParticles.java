package ar.com.kiritsu.multiparticlesim.tp3.logic;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public class StepParticles implements AreaStepFunction {

	private Supplier<Float> _dt;

	public StepParticles(Supplier<Float> dt) {
		_dt = dt;
	}

	@Override
	public Area apply(Area input) {
		float t = _dt.get();
		Preconditions.checkArgument(t >= 0);
		for (Particle particle : input.particles()) {
			Vector2f position = particle.position();
			Vector2f velocity = particle.velocity();
			position.x = position.x + velocity.x * t;
			position.y = position.y + velocity.y * t;
		}
		return input;
	}

}
