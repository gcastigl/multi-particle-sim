package ar.com.kiritsu.multiparticlesim.tp3.logic;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;

public class MirrorParticlesOnX implements AreaStepFunction {

	@Override
	public Area apply(Area input) {
		for (Particle particle : input.particles()) {
			Vector2f position = particle.position();
			float maxX = position.x + particle.radius();
			float minX = position.x - particle.radius();
			if (maxX >= input.bounds().x) {
				setVelocityX(particle, 1);
				position.x -= (input.bounds().x + particle.radius());
			} else if (minX < 0) {
				// setVelocityX(particle, -1);
				// position.x += (input.bounds().x + particle.radius());
			}
		}
		return input;
	}

	private void setVelocityX(Particle particle, float vx) {
		particle.velocity().set(vx, 0);
		particle.normalizeVelocity();
	}

}
