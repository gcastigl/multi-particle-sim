package ar.com.kiritsu.multiparticlesim.tp3;

import org.newdawn.slick.geom.Vector2f;

public class CollitionMatrix {

	private float _cn, _ct;

	public CollitionMatrix(float cn, float ct) {
		_cn = cn;
		_ct = ct;
	}

	public float ct() {
		return _ct;
	}

	public float cn() {
		return _cn;
	}

	public Vector2f multiply(Vector2f v, float angle, Vector2f output) {
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		float sinxcos = sin * cos;
		float cosSq = cos * cos;
		float sinSq = sin * sin;
		// Build matrix coefficients
		float m11 = -cn() * cosSq + ct() * sinSq;
		float m12 = -(ct() + cn()) * sinxcos;
		float m21 = m12;
		float m22 = -cn() * sinSq + ct() * cosSq;
		// output = M * v [2x1] = [2x2] * [2x1]
		float vx = m11 * v.x + m12 * v.y;
		float vy = m21 * v.x + m22 * v.y;
		output.set(vx, vy);
		return output;
	}

}
