package ar.com.kiritsu.multiparticlesim.tp3.logic;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionInformation;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionsInformation;

import com.google.common.base.Supplier;

public class SolveColitions implements AreaStepFunction {

	private Supplier<CollitionsInformation> _collitionsInfo;

	public SolveColitions setCollitionsInfo(Supplier<CollitionsInformation> collitions) {
		this._collitionsInfo = collitions;
		return this;
	}

	@Override
	public Area apply(Area input) {
		for (CollitionInformation collition : _collitionsInfo.get().collitions()) {
			collition.handler().handle(collition.subject());
		}
		return input;
	}

}
