package ar.com.kiritsu.multiparticlesim.tp3.logic;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionHandler;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionMatrix;

public class CollitionHandlers {

	public static final CollitionHandler wallCollitionHandler(final Wall wall) {
		return new CollitionHandler() {
			@Override
			public void handle(Particle particle) {
				Line bounds = wall.bounds();
				float[] normal = bounds.getNormal(0);
				Vector2f velocity = particle.velocity();
				if (normal[0] == 0) { // x == 0 => horizontal wall
					velocity.y = -velocity.y;
				} else if (normal[1] == 0) { // y == 0 => vertical wall
					velocity.x = -velocity.x;
				} else {
					throw new IllegalStateException("Not supported");
				}
			}
		};
	}

	public static final CollitionHandler particleParticleCollition(final float angle, final CollitionMatrix collitionMatrix) {
		return new CollitionHandler() {
			@Override
			public void handle(Particle particle) {
				Vector2f va = particle.velocity();
				collitionMatrix.multiply(va, angle, particle.velocity());
				particle.normalizeVelocity();
			}
		};
	}
}
