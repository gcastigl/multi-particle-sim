package ar.com.kiritsu.multiparticlesim.tp3;

import ar.com.kiritsu.multiparticlesim.Particle;

public class CollitionInformation {

	private Particle _subject;
	private CollitionHandler _handler;

	public CollitionInformation(Particle subject, CollitionHandler handler) {
		_subject = subject;
		_handler = handler;
	}

	public void applyHandler() {
		handler().handle(subject());
	}

	public CollitionHandler handler() {
		return _handler;
	}

	public Particle subject() {
		return _subject;
	}
}
