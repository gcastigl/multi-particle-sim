package ar.com.kiritsu.multiparticlesim.tp3.logic;

import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Wall;

import com.google.common.base.Supplier;

public class EmitParticles implements AreaStepFunction {

	private static final int MAX_TRIES = 10;

	private int _minParticles;
	private Supplier<Particle> _particleSupplier;
	private Supplier<Vector2f> _randomLocationSupplier;
	private FindNeighbors _findNeighbors;

	public EmitParticles(FindNeighbors findNeighbors) {
		_findNeighbors = findNeighbors;
	}

	public EmitParticles setMinParticles(int minParticles) {
		_minParticles = minParticles;
		return this;
	}

	public EmitParticles setParticleSupplier(Supplier<Particle> particleSupplier) {
		_particleSupplier = particleSupplier;
		return this;
	}

	public EmitParticles setRandomLocationSupplier(Supplier<Vector2f> randomLocationSupplier) {
		_randomLocationSupplier = randomLocationSupplier;
		return this;
	}

	public EmitParticles setFindNeighbors(FindNeighbors findNeighbors) {
		_findNeighbors = findNeighbors;
		return this;
	}

	@Override
	public Area apply(Area input) {
		if (input.particles().size() >= _minParticles) {
			return input;
		}
		int createAmount = _minParticles - input.particles().size();
		for (int i = 0; i < createAmount; i++) {
			_findNeighbors.at(input);	// XXX: each time a particle is added, neighbors MUST be recalculated
			Particle particle = _particleSupplier.get();
			boolean placed = false;
			int tries = 0;
			while (!placed && tries < MAX_TRIES) {
				particle.setPosition(_randomLocationSupplier.get());
				if (!hasCollitions(particle, input)) {
					placed = true;
					input.addParticle(particle);
				} else {
					tries++;
					if (tries == MAX_TRIES) {
						System.out.println("WARN: No se pudo agrgar particula. Densidad MUY alta.");
					}
				}
			}
		}
		return input;
	}

	private boolean hasCollitions(Particle particle, Area area) {
		Circle circle = new Circle(particle.position().x, particle.position().y, particle.radius());
		for (Wall wall : area.obstacles()) {
			if (wall.bounds().intersects(circle)) {
				return true;
			}
		}
		return !_findNeighbors.centeredOn(particle).find().isEmpty();
	}
}
