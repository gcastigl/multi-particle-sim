package ar.com.kiritsu.multiparticlesim.tp3;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class Simulation<T> {

	private boolean _started, _finished;
	private T _status;
	private Predicate<? super T> _cutCondition;
	private Function<T, T> _start = Functions.identity();
	private Function<T, T> _step = Functions.identity();
	private Function<T, T> _end = Functions.identity();

	public Simulation() {
		_started = false;
		_finished = false;
	}

	public Simulation<T> setCutCondition(Predicate<? super T> cutCondition) {
		_cutCondition = cutCondition;
		return this;
	}

	public Simulation<T> onStart(Function<T, T> callback) {
		_start = Functions.compose(callback, _start);
		return this;
	}

	public Simulation<T> onStep(Function<T, T> step) {
		_step = Functions.compose(step, _step);
		return this;
	}

	public Simulation<T> onEnd(Function<T, T> callback) {
		_end = Functions.compose(callback, _end);
		return this;
	}

	public Simulation<T> setInitialState(T status) {
		_status = status;
		return this;
	}

	public void step() {
		Preconditions.checkState(!isFinished());
		if (!_started) {
			setStatus(_start.apply(status()));
			_started = true;
		}
		setStatus(_step.apply(status()));
		boolean finished = _cutCondition.apply(status());
		if (finished) {
			setFinished();
		}
	}

	public boolean isFinished() {
		return _finished;
	}

	public void setFinished() {
		_finished = true;
		_end.apply(status());
	}

	public T status() {
		return _status;
	}

	public void setStatus(T status) {
		_status = Preconditions.checkNotNull(status);
	}
}
