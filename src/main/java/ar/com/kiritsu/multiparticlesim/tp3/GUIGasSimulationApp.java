package ar.com.kiritsu.multiparticlesim.tp3;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.AreaRender;
import ar.com.kiritsu.multiparticlesim.Lines;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.log.AreaFilesHandler;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep;
import ar.com.kiritsu.multiparticlesim.log.DynamicFileStep.ParticleStatus;
import ar.com.kiritsu.multiparticlesim.log.StaticFile.ParticleDesc;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

public class GUIGasSimulationApp extends BasicGame {

	public static void main(String[] args) throws Exception {
		// GasSimulationApp.main(args);
		File staticFile = new File("src/main/resources/tp3/static.txt");
		File dynamicFile = new File("src/main/resources/tp3/dynamic.txt");
		File areaFile = new File("src/main/resources/tp3/area.txt");
		AppGameContainer appContainer = new AppGameContainer(new GUIGasSimulationApp(staticFile, dynamicFile, areaFile));
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(700, 700, false);
		appContainer.start();
	}

	private static final float TIME_STEP = 1f / 100f;

	private final File _staticFile;
	private final File _dynamicFile;
	private final File _areaFile;

	private AreaRender _renderer;
	private Simulation<Area> _simulation;
	private AreaFilesHandler _areaFileHandler;
	private DynamicFileStep _currentStep;

	public GUIGasSimulationApp(File staticFile, File dynamicFile, File areaFile) {
		super("Gas particles simulation");
		_staticFile = staticFile;
		_dynamicFile = dynamicFile;
		_areaFile = areaFile;
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		container.setTargetFrameRate(50);
		_renderer = new AreaRender().setScale(600);
		try {
			_areaFileHandler = new AreaFilesHandler(_staticFile, _dynamicFile, _areaFile);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		_simulation = new Simulation<Area>().setCutCondition(new Predicate<Area>() {
			@Override
			public boolean apply(Area input) {
				return _currentStep == null;
			}
		});
		_simulation.setInitialState(setupArea(new Area()));
		_simulation.onStep(updateCurrentStep());
	}

	private Area setupArea(Area area) {
		Vector2f bounds = new Vector2f(_areaFileHandler.areaSize(), _areaFileHandler.areaSize());
		for (Line wallDef : _areaFileHandler.getWallsDef()) {
			area.addObstale(new Wall().setBounds(wallDef));
		}
		area.setBounds(bounds);
		// Initialize all particles at start location
		_currentStep = _areaFileHandler.nextSteps();
		int id = 0;
		for (ParticleDesc desc : _areaFileHandler.particlesDesc()) {
			ParticleStatus status = _currentStep.particles().get(id);
			Preconditions.checkState(id == status.id());
			area.particles().add(
				new Particle(id++, desc.radius())
					.setPosition(status.center())
					.setVelocity(status.velocity())
			);
		}
		area.incElapsedTime(_currentStep.endTime());
		return area;
	}

	private Function<Area, Area> updateCurrentStep() {
		return new Function<Area, Area>() {

			private final Map<Particle, Line> paths = Maps.newHashMap();
			private final Vector2f _tmpCenter = new Vector2f();
			
			@Override
			public Area apply(Area input) {
				input.incElapsedTime(TIME_STEP);
				float p = 0;
				if (_currentStep.duration() != 0) {
					p = (input.elapsedTime() - _currentStep.startTime()) / _currentStep.duration();
				}
				if (_currentStep.duration() == 0 || p >= 1) {
					DynamicFileStep prevStep = _currentStep;
					input.setElapsedTime(_currentStep.endTime());
					_currentStep = _areaFileHandler.nextSteps();
					if (_currentStep == null) { // No more steps avaliable....
						return input;
					}
					int particlesCount = _currentStep.particles().size();
					// Set system set to end location to prevent floating point errors!
					for (int i = 0; i < particlesCount; i++) {
						Particle particle = Iterables.get(input.particles(), i);
						ParticleStatus expectedStatus = _currentStep.particles().get(i);
						if (_currentStep.duration() == 0) {
							particle.setPosition(expectedStatus.center());
							continue;
						}
						Line path = paths.get(particle);
						if (path == null) {
							paths.put(particle, path = new Line(0, 0));
						}
						ParticleStatus prevStatus = prevStep.particles().get(i);
						path.set(prevStatus.center(), expectedStatus.center());
						Preconditions.checkArgument(particle.id() == expectedStatus.id());
						Preconditions.checkArgument(particle.id() == prevStatus.id());
					}
				} else {
					// Inc one step
					Preconditions.checkArgument(0 <= p);
					for (Particle particle : input.particles()) {
						Line path = paths.get(particle);
						Lines.ponintAt(path, p, _tmpCenter);
						particle.setPosition(_tmpCenter);
					}
				}
				return input;
			}
		};
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		_renderer.render(container, g, _simulation.status());
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		if (!_simulation.isFinished()) {
			_simulation.step();
		} else {
			System.out.println("Simulation already finished!!");
		}
		if (container.getInput().isKeyPressed(Input.KEY_R)) {
			container.reinit();
		}
		if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
			_areaFileHandler.close();
			container.exit();
		}
	}
}
