package ar.com.kiritsu.multiparticlesim.tp3;

import ar.com.kiritsu.multiparticlesim.Particle;

public interface CollitionHandler {

	void handle(Particle particle);

}
