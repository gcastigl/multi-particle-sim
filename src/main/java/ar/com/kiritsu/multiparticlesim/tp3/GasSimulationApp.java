package ar.com.kiritsu.multiparticlesim.tp3;

import static ar.com.kiritsu.math.RandomUtil.random;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.ElapsedTimeIsGreaterThan;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;
import ar.com.kiritsu.multiparticlesim.IncrementElapsedTime;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Vectors2f;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.log.LogAreaToFile;
import ar.com.kiritsu.multiparticlesim.log.LogCollitionTime;
import ar.com.kiritsu.multiparticlesim.tp3.logic.CalcCollitions;
import ar.com.kiritsu.multiparticlesim.tp3.logic.EmitParticles;
import ar.com.kiritsu.multiparticlesim.tp3.logic.MirrorParticlesOnX;
import ar.com.kiritsu.multiparticlesim.tp3.logic.SolveColitions;
import ar.com.kiritsu.multiparticlesim.tp3.logic.StepParticles;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

public class GasSimulationApp {

	public static void main(String[] args) throws Exception {
		String dir = "src/main/resources/tp3/";
		new File(dir).mkdirs();
		File dynamicFile = new File(dir + "dynamic.txt");
		File areaFile = new File(dir + "area.txt");
		File collitiomTimeFile = new File(dir + "collitionTime.txt");
		new GasSimulationApp(dynamicFile, areaFile, collitiomTimeFile).run();
	}

	private final File _dynamicFile;
	private final File _areaFile;
	private final File _collitionTimeFile;

	private Writer _dynamicWriter, _areaWriter, _collitionTimeWriter;

	public GasSimulationApp(File dynamicFile, File areaFile, File collitionTimeFile) {
		_dynamicFile = dynamicFile;
		_areaFile = areaFile;
		_collitionTimeFile = collitionTimeFile;
	}

	public void run() throws IOException {
		_dynamicWriter = new BufferedWriter(new FileWriter(_dynamicFile));
		_areaWriter = new BufferedWriter(new FileWriter(_areaFile));
		_collitionTimeWriter = new BufferedWriter(new FileWriter(_collitionTimeFile));
		final float MAX_SIM_TIME = 10;
		Simulation<Area> simulation = new Simulation<Area>().setCutCondition(new ElapsedTimeIsGreaterThan(MAX_SIM_TIME));
		Area area = new Area().setBounds(new Vector2f(1, 0.5f));
		float L = 0.1f;
		float wallYOffset = (area.bounds().y - L) / 2;
		float wallXOffset = 0.4f;
		area
			.addObstale(new Wall(new Line(0, 0, area.bounds().x, 0)))
			.addObstale(new Wall(new Line(0, area.bounds().y, area.bounds().x, area.bounds().y)))
			// Wall to avoid particles from escaping along the X axis
			.addObstale(new Wall(new Line(0f, 0, 0f, area.bounds().y)).setTraspassableDir(Vectors2f.X()))
			.addObstale(new Wall(new Line(area.bounds().x + 0.05f, 0, area.bounds().x + 0.05f, area.bounds().y)))
			// Wall in the middle
			.addObstale(new Wall(new Line(new Vector2f(wallXOffset, wallYOffset), new Vector2f(wallXOffset, wallYOffset + L))))
		;
		FindNeighbors findNeighbors = new FindNeighbors(15);
		simulation.setInitialState(area);
		CalcCollitions calcCollitions = new CalcCollitions(new CollitionMatrix(1f, 1f));
		Supplier<CollitionsInformation> collitions = calcCollitions.collitionsInfo();
		Supplier<Float> minCollitionTime = GetMinCollitionTime.of(collitions);
		LogAreaToFile logAreaToFile = logToFile();
		simulation
			.onStep(emitParticles(findNeighbors, simulation))
			.onStep(calcCollitions)
			.onStep(new StepParticles(minCollitionTime))
			.onStep(new SolveColitions().setCollitionsInfo(collitions))
			.onStep(logAreaToFile)
			.onStep(new MirrorParticlesOnX())
			.onStep(logAreaToFile)
			.onStep(new LogCollitionTime(_collitionTimeWriter, minCollitionTime))
			.onStep(incrementElspasedTime(collitions))
//			.then(logAreaToFile)
//			.then(new RecalculateNeighbors(findNeighbors))
			.onStep(new Function<Area, Area>() {
				@Override
				public Area apply(Area input) {
//					float p = 100 * input.elapsedTime() / MAX_SIM_TIME;
//					System.out.println(p);
					return input;
				}
			})
		;
		while (!simulation.isFinished()) {
			simulation.step();
		}
		_dynamicWriter.close();
		_areaWriter.close();
		_collitionTimeWriter.close();
	}

	private EmitParticles emitParticles(FindNeighbors findNighbors, final Simulation<Area> simulation) {
		return new EmitParticles(findNighbors).setParticleSupplier(new Supplier<Particle>() {

			private final float expectedVelocity = 0.2f;
			private final Vector2f velocity = new Vector2f(1f, 0f).normalise().scale(expectedVelocity);
			private final float RADIUS = 0.005f;
			private int _lastId = 0;

			@Override
			public Particle get() {
				return new Particle(_lastId++, RADIUS).setVelocity(velocity).setExpectedVelocity(expectedVelocity);
			}
		}).setRandomLocationSupplier(new Supplier<Vector2f>() {
			@Override
			public Vector2f get() {
				float y = simulation.status().bounds().y;
				float x = simulation.status().bounds().x;
				return new Vector2f(random(0, x), random(0, y));
			}
		}).setMinParticles(100);
	}

	private LogAreaToFile logToFile() {
		return new LogAreaToFile(_dynamicWriter, _areaWriter);
	}

	private Function<Area, Area> incrementElspasedTime(final Supplier<CollitionsInformation> collitionsInfo) {
		return new IncrementElapsedTime(new Supplier<Float>() {
			@Override
			public Float get() {
				return collitionsInfo.get().minCollitionTime();
			}
		});
	}
}
