package ar.com.kiritsu.multiparticlesim.tp3;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

public class GetMinCollitionTime implements Function<CollitionsInformation, Float> {

	public static Supplier<Float> of(Supplier<CollitionsInformation> collitionsInformation) {
		return Suppliers.compose(new GetMinCollitionTime(), collitionsInformation);
	}

	@Override
	public Float apply(CollitionsInformation input) {
		return input.minCollitionTime();
	}

}
