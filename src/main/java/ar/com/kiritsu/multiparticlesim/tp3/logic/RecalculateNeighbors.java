package ar.com.kiritsu.multiparticlesim.tp3.logic;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindNeighbors;

public class RecalculateNeighbors implements AreaStepFunction {

	private FindNeighbors _findNeighbors;

	public RecalculateNeighbors(FindNeighbors findNeighbors) {
		_findNeighbors = findNeighbors;
	}

	@Override
	public Area apply(Area input) {
		_findNeighbors.at(input);
		return input;
	}

}
