package ar.com.kiritsu.multiparticlesim.tp3.logic;

import static ar.com.kiritsu.multiparticlesim.Vectors2f.versor;
import static ar.com.kiritsu.multiparticlesim.tp3.logic.CollitionHandlers.particleParticleCollition;
import static ar.com.kiritsu.multiparticlesim.tp3.logic.CollitionHandlers.wallCollitionHandler;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.SegmentIntersection;
import ar.com.kiritsu.multiparticlesim.Vectors2f;
import ar.com.kiritsu.multiparticlesim.Wall;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionHandler;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionInformation;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionMatrix;
import ar.com.kiritsu.multiparticlesim.tp3.CollitionsInformation;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public class CalcCollitions implements AreaStepFunction {

	private final CollitionsInformation _collitions = new CollitionsInformation();
	private final CollitionMatrix _collitionMatrix;
	private final SegmentIntersection segmentIntersection = new SegmentIntersection();

	public CalcCollitions(CollitionMatrix collitionMatrix) {
		_collitionMatrix = Preconditions.checkNotNull(collitionMatrix);
		_collitions.clear();
	}

	public Supplier<CollitionsInformation> collitionsInfo() {
		return new Supplier<CollitionsInformation>() {
			@Override
			public CollitionsInformation get() {
				return _collitions;
			}
		};
	}

	@Override
	public Area apply(Area input) {
		_collitions.clear();
		Vector2f predicatedPathEnd = new Vector2f();
		for (Particle particle : input.particles()) {
			Vectors2f.add(particle.position(), particle.velocity(), 100000, predicatedPathEnd);
			Line predictedPath = new Line(particle.position(), predicatedPathEnd);
			for (Wall wall : input.obstacles()) {
				wallToParticleCollitions(wall, particle, predictedPath);
			}
			for (Particle other : input.particles()) {
				if (other != particle) {
					particleToParticleCollition(particle, other);
				}
			}
		}
		return input;
	}

	private void wallToParticleCollitions(Wall wall, Particle particle, Line path) {
		if (wall.traspassableDir().isPresent()) {
			float dot = particle.velocity().dot(wall.traspassableDir().get());
			if (dot > 0) {	// XXX: wall is traspassable, no collition
				return;
			}
		}
		Vector2f collitionPos = segmentIntersection.at(wall.bounds(), path);
		if (collitionPos == null) {
			return;
		}
		float distance = particle.position().distance(collitionPos) - particle.radius();
		distance = Math.max(0, distance);	// prevent negative numbers because of floating point error
		float collitionTime = distance / particle.velocity().length();
		
		if (_collitions.updateMinCollitionTime(collitionTime)) {
			addCollition(particle, wallCollitionHandler(wall));
		}
	}

	private final Vector2f _en = new Vector2f();
	private final Vector2f _dv = new Vector2f();
	private final Vector2f _dr = new Vector2f();

	private void particleToParticleCollition(Particle p1, Particle p2) {
		_dv.set(p1.velocity());
		_dv.sub(p2.velocity());
		_dr.set(p1.position());
		_dr.sub(p2.position());
		float dvdr = _dv.dot(_dr);
		if (dvdr >= 0) {
			return;
		}
		float drdr = _dr.dot(_dr);
		float dvdv = _dv.dot(_dv);
		float sigma = p1.radius() + p2.radius();
		float d = (dvdr * dvdr) - dvdv * (drdr - sigma * sigma);
		if (d < 0) {
			return;
		}
		float tc = -(dvdr + (float) Math.sqrt(d)) / dvdv;
		if (tc < 0) {
			System.out.println("[WARN] neg tc = " + tc);
			tc = 0;
		}
		if (_collitions.updateMinCollitionTime(tc)) {
			versor(p2.position(), p1.position(), _en);
			float angle = (float) Math.toRadians(_en.getTheta());
			addCollition(p1, particleParticleCollition(angle, _collitionMatrix));
		}
	}

	private void addCollition(Particle p1, CollitionHandler handler) {
		_collitions.addCollition(new CollitionInformation(p1, handler));
	}
}
