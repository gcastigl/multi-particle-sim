package ar.com.kiritsu.multiparticlesim;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import ar.com.kiritsu.block.Block;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FindAllCollitions {

	private FindNeighbors _findNeighbors;

	public FindAllCollitions(Optional<? extends Block<Particle>> outOfBoundsHandler) {
		_findNeighbors = new FindNeighbors(10, outOfBoundsHandler.orNull());
	}

	public Collection<CollitionEvent> at(Area input) {
		Collection<CollitionEvent> collitionEvents = Lists.newLinkedList();
		_findNeighbors.at(input);
		Iterator<Particle> particlesIt = input.particles().iterator();
		Set<Particle> collitioning = Sets.newHashSet();
		while (particlesIt.hasNext()) {
			Particle particle = particlesIt.next();
			if (collitioning.contains(particle)) {
				continue;
			}
			Collection<Particle> collitions = _findNeighbors.centeredOn(particle).find();
			if (!collitions.isEmpty()) {
				collitionEvents.add(new CollitionEvent(particle, collitions));
				collitioning.add(particle);
				collitioning.addAll(collitions);
			}
		}
		return collitionEvents;
	}

	public static class CollitionEvent {
		private Collection<Particle> _collitions = Lists.newArrayList();

		public CollitionEvent(Particle subject, Collection<Particle> collitions) {
			_collitions.addAll(collitions);
			_collitions.add(subject);
		}

		public Collection<Particle> particles() {
			return _collitions;
		}

	}
}
