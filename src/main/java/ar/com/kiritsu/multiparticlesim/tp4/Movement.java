package ar.com.kiritsu.multiparticlesim.tp4;

import org.apache.commons.lang3.StringUtils;
import org.newdawn.slick.geom.Vector2f;

import com.google.common.base.Preconditions;

public class Movement {

	private final int _orders;
	private final Vector2f[] _rs;

	public Movement(int orders) {
		_orders = orders + 1;
		_rs = new Vector2f[_orders];
		for (int i = 0; i < _orders; i++) {
			_rs[i] = new Vector2f();
		}
	}

	public Movement addRs(Vector2f[] rs) {
		Preconditions.checkArgument(rs.length == _rs.length);
		for (int i = 0; i < rs.length; i++) {
			_rs[i].add(rs[i]);
		}
		return this;
	}

	public Movement setRs(Vector2f[] rs) {
		Preconditions.checkArgument(rs.length == _rs.length);
		int index = 0;
		for (Vector2f r : rs) {
			setR(index++, r);
		}
		return this;
	}

	public Movement setR(int order, Vector2f value) {
		_rs[order].set(value);
		return this;
	}

	public Vector2f[] rs() {
		return _rs;
	}

	public Vector2f r(int order) {
		return _rs[order];
	}

	public Vector2f position() {
		return r(0);
	}

	public Vector2f velocity() {
		return r(1);
	}

	public Vector2f acceleration() {
		return r(2);
	}

	@Override
	public String toString() {
		return StringUtils.join(_rs, "\n");
	}
}
