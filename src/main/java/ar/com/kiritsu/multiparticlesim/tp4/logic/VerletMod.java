package ar.com.kiritsu.multiparticlesim.tp4.logic;

import static ar.com.kiritsu.multiparticlesim.Vectors2f.add;
import static ar.com.kiritsu.multiparticlesim.Vectors2f.scale;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.tp3.logic.AreaStepFunction;

public class VerletMod implements AreaStepFunction {

	private final float _dt;

	public VerletMod(float dt) {
		_dt = dt;
	}

	@Override
	public Area apply(Area input) {
		for (Particle particle : input.particles()) {
			step(particle);
		}
		return input;
	}

	private void step(Particle particle) {
		Vector2f f = particle.appliedForce();
		Vector2f r = particle.position();
		Vector2f v = particle.velocity();
		Vector2f vn = add(v, scale(f, _dt / particle.mass()));
		Vector2f rn = add(
			r, 
			scale(vn, _dt), 
			scale(f, _dt * _dt * 2 / particle.mass())
		);
		particle.position().set(rn);
		particle.velocity().set(vn);
	}

}
