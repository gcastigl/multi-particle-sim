package ar.com.kiritsu.multiparticlesim.tp4.force;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;

public abstract class ForceEmitter extends Block<Area> {

	public abstract EnergyValue energy(Area input);

}
