package ar.com.kiritsu.multiparticlesim.tp4.logic;

import static ar.com.kiritsu.multiparticlesim.Vectors2f.add;
import static ar.com.kiritsu.multiparticlesim.Vectors2f.scale;

import java.util.Map;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.tp3.logic.AreaStepFunction;

import com.google.common.collect.Maps;

public class BeemanStep implements AreaStepFunction {

	private final float _dt;
	private final float _dtSq;
	private Map<Particle, Vector2f> _prevAByParticle = Maps.newHashMap();

	public BeemanStep(float dt) {
		_dt = dt;
		_dtSq = _dt * _dt;
	}

	@Override
	public Area apply(Area input) {
		for (Particle particle : input.particles()) {
			step(particle);
		}
		return input;
	}

	private void step(Particle particle) {
		Vector2f r = particle.position();
		Vector2f v = particle.velocity();
		Vector2f a = particle.acceleration();
		Vector2f an_1 = prevA(particle);
		Vector2f updatedR = add(
			r, 
			scale(v, _dt), 
			scale(a, (_dtSq * 2) / 3), 
			scale(an_1, -_dtSq / 6)
		);
		Vector2f updatedV = add(
			v,
			scale(a, _dt * 7 / 6f), 
			scale(an_1, -_dt / 6)
		);
		particle.position().set(updatedR);
		particle.velocity().set(updatedV);
		particle.acceleration().set(scale(particle.appliedForce(), 1 / particle.mass()));
	}

	private Vector2f prevA(Particle particle) {
		Vector2f prevA = _prevAByParticle.get(particle);
		if (prevA == null) {
			_prevAByParticle.put(particle, prevA = new Vector2f());
		}
		return prevA;
	}
}
