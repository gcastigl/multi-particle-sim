package ar.com.kiritsu.multiparticlesim.tp4.force;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;
import ar.com.kiritsu.multiparticlesim.Particle;

public class GravityForce extends ForceEmitter {

	private static final float G = 9.8f;
	private final float _zero;

	public GravityForce() {
		this(0f);
	}
	
	public GravityForce(float zero) {
		_zero = zero;
	}
	
	@Override
	public EnergyValue energy(Area input) {
		float potencial = 0;
		for (Particle particle : input.particles()) {
			float y = Math.abs(particle.position().y - _zero);
			potencial += particle.mass() * G * y;
		}
		return new EnergyValue(0, potencial);
	}

	@Override
	protected void exec(Area input) {
		Vector2f force = new Vector2f();
		for (Particle particle : input.particles()) {
			particle.appliedForce().add(force.scale(particle.mass()));
		}
	}

}
