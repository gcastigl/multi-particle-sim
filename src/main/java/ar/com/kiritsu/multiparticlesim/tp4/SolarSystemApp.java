package ar.com.kiritsu.multiparticlesim.tp4;

import static ar.com.kiritsu.math.FloatUtil.sum;
import static ar.com.kiritsu.multiparticlesim.Vectors2f.avgWeightened;
import static ar.com.kiritsu.multiparticlesim.verlet.Particles.group;
import static ar.com.kiritsu.multiparticlesim.verlet.Particles.mass;
import static ar.com.kiritsu.multiparticlesim.verlet.Particles.position;
import static ar.com.kiritsu.multiparticlesim.verlet.Particles.velocity;
import static com.google.common.collect.Collections2.transform;

import java.util.Collection;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.math.RandomUtil;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.FindAllCollitions;
import ar.com.kiritsu.multiparticlesim.FindAllCollitions.CollitionEvent;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.SimulationApp;
import ar.com.kiritsu.multiparticlesim.Vectors2f;
import ar.com.kiritsu.multiparticlesim.tp2.TraceParticleSteps;
import ar.com.kiritsu.multiparticlesim.tp4.force.GravitationalForce;
import ar.com.kiritsu.multiparticlesim.tp4.logic.AreaForcesUpdater;
import ar.com.kiritsu.multiparticlesim.tp4.logic.GearPredictorStep;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

public class SolarSystemApp extends SimulationApp {

	public static void main(String[] args) throws SlickException {
		AppGameContainer appContainer = new AppGameContainer(new SolarSystemApp());
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(1366, 768, false);
		appContainer.start();
	}

	private static final int PLANET_GROUP = 0;
	private static final int SUN_GROUP = 1; 

	private static final float TIME_STEP = 1 / 100f;
	private final int _order = 5;

	public SolarSystemApp() {
		super("Sistema solar");
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		super.init(container);
		renderer()
			.setParticleColor(Functions.constant(Color.red))
			.setParticleImage(new Function<Particle, Optional<Image>>() {
				private Optional<Image> sun = Optional.of(new Image("./src/main/resources/sun.gif"));
				@Override
				public Optional<Image> apply(Particle input) {
					if (input.group() == SUN_GROUP) {
						return sun;
					}
					return Optional.absent();
				}
			})
			.setScale(0.2f).setBackground(new Image("./src/main/resources/space-wallpaper.jpg"))
//			.setPostRender(new ShowEnergyOnArea())
			;
		Area area = new Area().setBounds(new Vector2f(10000, 10000));
		simulation()
			.setInitialState(area)
			.setCutCondition(Predicates.alwaysFalse())
			.onStart(new Block<Area>() { 
				@Override 
				public void exec(Area input) { setup(input); }
			})
			.onStep(new GearPredictorStep(TIME_STEP))
//			.onStep(new BeemanStep(TIME_STEP))
//			.onStep(new VerletMod(TIME_STEP))
			.onStep(new Block<Area>() {
				private FindAllCollitions findCollitions = new FindAllCollitions(Optional.of(new OutOfBoundsHandler()));
				@Override
				protected void exec(Area input) {
					mergeCollidingParticles(input, findCollitions.at(input), true);
				}
			})
			.onStep(new AreaForcesUpdater())
			.onStep(new TraceParticleSteps().setTrailSize(20).setDelaySteps(3));
		;
	}

	private void setup(Area input) {
		input.forces().add(new GravitationalForce());
		// Create initial planets
		Vector2f sunCenter = new Vector2f(3800, 3800);
		renderer().offset().set(sunCenter);
		renderer().offset().scale(-renderer().scale() * 0.5f);
		Particle sun = new Particle(0, 10, 1E15f, _order).setPosition(sunCenter).setRadius(new SphereRadius(1400)).setGroup(SUN_GROUP);
		float minDistance = 200;
		final float orbitDistance = 500;
		final float velocityMod = 80;
		input.addParticle(sun);
		float ro = 500;
		Vector2f dir = new Vector2f();
		for (int i = 0; i < 20; i++) {
			float mass = 1E10f;
			dir.set(RandomUtil.random(-1, 1), RandomUtil.random(-1, 1));
			dir.normalise().scale(minDistance + RandomUtil.random(0, orbitDistance));
			Vector2f planetCenter = new Vector2f(sunCenter).add(dir);
			float distance = planetCenter.distance(sunCenter);
			float planetVelocityMod = RandomUtil.random(velocityMod, velocityMod) / (0.005f * distance + 1);
			Vectors2f.versor(sunCenter, planetCenter, dir);
			Vector2f velocity = dir.getPerpendicular().scale(planetVelocityMod);
			input.addParticle(
				new Particle(i + 1, 0, mass, _order)
					.setRadius(new SphereRadius(ro))
					.setPosition(planetCenter)
					.setVelocity(velocity)
					.setGroup(PLANET_GROUP)
			);
		}
	}

	private void mergeCollidingParticles(Area area, Collection<CollitionEvent> collitionEvents, boolean joinOnCollition) {
		if (!joinOnCollition) {
			return;
		}
		for (CollitionEvent collitionEvent : collitionEvents) {
			area.removeParticles(collitionEvent.particles());
			Collection<Float> masses = transform(collitionEvent.particles(), mass());
			Vector2f avgPosition = avgWeightened(transform(collitionEvent.particles(), position()), masses);
			Vector2f ponderedVelocity = avgWeightened(transform(collitionEvent.particles(), velocity()), masses);
			float totalMass = sum(masses);
			Particle first = Iterables.get(collitionEvent.particles(), 0);	// re use any of the just removed particles
			first.setPosition(avgPosition).setVelocity(ponderedVelocity).setMass(totalMass);
			area.addParticle(first);
			if (Iterables.any(transform(collitionEvent.particles(), group()), Predicates.equalTo(SUN_GROUP))) {
				first.setGroup(SUN_GROUP);
			} else {
				first.setGroup(PLANET_GROUP);
			}
		}
	}
	
	private static final class SphereRadius implements Function<Particle, Float> {

		private final float _ro;
		private float _lastMass;
		private float _r;

		public SphereRadius(float ro) {
			_ro = ro;
		}

		@Override
		public Float apply(Particle input) {
			float mass = input.mass();
			if (_lastMass == mass) {
				return _r;
			}
			_lastMass = mass;
			float aux = (float) ((3 * _lastMass) / (_ro * 4 * Math.PI));
			_r = (float) Math.pow(aux, 1 / 3f) / 100;
			return _r;
		}
		
	}
	
	private static final class OutOfBoundsHandler extends Block<Particle> {

		@Override
		protected void exec(Particle input) {
			System.out.println("Fuera del mapa: " + input.id());
		}
		
	}
}
