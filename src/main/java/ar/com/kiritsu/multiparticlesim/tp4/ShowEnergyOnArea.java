package ar.com.kiritsu.multiparticlesim.tp4;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import ar.com.kiritsu.math.FloatUtil;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;
import ar.com.kiritsu.slick.Renderer;

import com.google.common.collect.Lists;

public class ShowEnergyOnArea implements Renderer<Area> {

	private AreaTotalEnergy _calcEnergy = new AreaTotalEnergy();
	private List<Float> _lastCinematicMeditions = Lists.newArrayList();
	private List<Float> _lastPotentialMeditions = Lists.newArrayList();

	@Override
	public void render(GameContainer container, Graphics g, Area value) throws SlickException {
		if (_lastCinematicMeditions.size() >= 60) {
			_lastCinematicMeditions.remove(0);
			_lastPotentialMeditions.remove(0);
		}
		g.setColor(Color.red);
		EnergyValue energy = _calcEnergy.apply(value);
		_lastCinematicMeditions.add(energy.cinematic());
		_lastPotentialMeditions.add(energy.potential());
		float avgCinenatic = FloatUtil.avg(_lastCinematicMeditions);
		float avgPotential = FloatUtil.avg(_lastPotentialMeditions);
		String currEnergyString = String.format("[CUR] %.2f + %.2f = %.2f", avgCinenatic, avgPotential, avgCinenatic + avgPotential);
		g.drawString(currEnergyString, 10, container.getHeight() - 80);
	}

}
