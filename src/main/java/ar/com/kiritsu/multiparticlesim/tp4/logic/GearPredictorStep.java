package ar.com.kiritsu.multiparticlesim.tp4.logic;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.math.MathUtil;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.tp3.logic.AreaStepFunction;
import ar.com.kiritsu.multiparticlesim.tp4.Movement;

public class GearPredictorStep implements AreaStepFunction {

	//a0=3/16, a1=325/360, a2=1, a3=11/18, a4=1/6 , and a5=1/60.
	private static final float[][] ALPHAS_BY_ORDER = new float[][] {
		null, 
		null, 
		{ 0f, 1f, 1f }, 
		{ 1 / 6f, 5 / 6f, 1f, 1 / 3f },
		{ 19 / 90f, 3 / 4f, 1f, 1 / 2f, 1 / 12f },
		{ 3 / 16f, 251 / 360f, 1f, 11 / 18f, 1 / 6f, 1 / 60f }
//		{ 19 / 120f, 3 / 4f, 1f, 1 / 2f, 1 / 12f },
//		{ 3 / 20f, 251 / 360f, 1f, 11 / 18f, 1 / 6f, 1 / 60f } 
	};
	private final int _order;
	private final float _dt;

	public GearPredictorStep(float dt) {
		_dt = dt;
		_order = 5;
	}

	@Override
	public Area apply(Area input) {
		Vector2f dr2 = new Vector2f();
		for (Particle particle : input.particles()) {
			Movement current = particle.movementStatus();
			Movement predicted = predict(current);
			calcDr2(particle, dr2);
			Movement corrected = correct(predicted, dr2);
			particle.movementStatus().setRs(corrected.rs());
		}
		return input;
	}

	public Movement predict(Movement steps) {
		Movement prediction = new Movement(_order);
		Vector2f tmp = new Vector2f();
		// 0
		tmp.set(0, 0);
		tmp
			.add(steps.r(0))
			.add(steps.r(1).copy().scale(_dt))
			.add(steps.r(2).copy().scale(_dt * _dt / 2))
			.add(steps.r(3).copy().scale(_dt * _dt * _dt / 6))
			.add(steps.r(4).copy().scale(_dt * _dt * _dt * _dt / 24))
			.add(steps.r(5).copy().scale(_dt * _dt * _dt * _dt * _dt / 120))
		;
		prediction.r(0).set(tmp);
		// 1
		tmp.set(0, 0);
		tmp
			.add(steps.r(1))
			.add(steps.r(2).copy().scale(_dt))
			.add(steps.r(3).copy().scale(_dt * _dt / 2))
			.add(steps.r(4).copy().scale(_dt * _dt * _dt / 6))
			.add(steps.r(5).copy().scale(_dt * _dt * _dt * _dt / 24))
		;
		prediction.r(1).set(tmp);
		// 2
		tmp.set(0, 0);
		tmp
			.add(steps.r(2))
			.add(steps.r(3).copy().scale(_dt))
			.add(steps.r(4).copy().scale(_dt * _dt / 2))
			.add(steps.r(5).copy().scale(_dt * _dt * _dt / 6))
		;
		prediction.r(2).set(tmp);
		// 3
		tmp.set(0, 0);
		tmp
			.add(steps.r(3))
			.add(steps.r(4).copy().scale(_dt))
			.add(steps.r(5).copy().scale(_dt * _dt / 2))
		;
		prediction.r(3).set(tmp);
		// 4
		tmp.set(0, 0);
		tmp
			.add(steps.r(4))
			.add(steps.r(5).copy().scale(_dt))
		;
		prediction.r(4).set(tmp);
//		// 5
		tmp.set(0, 0);
		tmp
			.add(steps.r(5))
		;
		prediction.r(5).set(tmp);
		return prediction;
	}

	private void calcDr2(Particle particle, Vector2f result) {
		result.set(particle.appliedForce());
		result.scale(1 / particle.mass()).sub(particle.movementStatus().r(2));
		result.scale(_dt * _dt / 2);
	}

	private Movement correct(Movement prediction, Vector2f dr2) {
		Movement corrected = new Movement(_order);
		Vector2f tmp = new Vector2f();
		float[] alphas = ALPHAS_BY_ORDER[5];
		float magic = 60;
		// 0
		tmp.set(0, 0);
		tmp
			.add(prediction.r(0))
			.add(dr2.copy().scale(alphas[0]))
		;
		corrected.r(0).set(tmp);
		// 1
		tmp.set(0, 0);
		tmp
			.add(prediction.r(1))
			.add(dr2.copy().scale(alphas[1] / _dt))
		;
		corrected.r(1).set(tmp);
		// 2
		tmp.set(0, 0);
		tmp
			.add(prediction.r(2))
			.add(dr2.copy().scale(alphas[2] * 2 / (_dt * _dt)))
		;
		corrected.r(2).set(cap(tmp, magic));
		// 3
		tmp.set(0, 0);
		tmp
			.add(prediction.r(3))
			.add(dr2.copy().scale(alphas[3] * 6 / (_dt * _dt * _dt)))
		;
		corrected.r(3).set(cap(tmp, magic + 30));
		 // 4
		tmp.set(0, 0);
		tmp
			.add(prediction.r(4))
			.add(dr2.copy().scale(alphas[4] * 24 / (_dt * _dt * _dt * _dt)))
		;
		corrected.r(4).set(cap(tmp, magic + 40));
		// 5
		tmp.set(0, 0);
		tmp
			.add(prediction.r(5))
			.add(dr2.copy().scale(alphas[5] * 120 / (_dt * _dt * _dt * _dt * _dt)))
		;
		corrected.r(5).set(cap(tmp, magic + 50));
		return corrected;
	}

	private Vector2f cap(Vector2f v, float mod) {
		v.x = cap(v.x, mod);
		v.y = cap(v.y, mod);
		return v;
	}

	private float cap(double n, float mod) {
		return (float) MathUtil.capped((float) n, -mod, mod);
	}
}
