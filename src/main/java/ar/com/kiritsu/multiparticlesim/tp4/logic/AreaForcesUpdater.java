package ar.com.kiritsu.multiparticlesim.tp4.logic;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Vectors2f;
import ar.com.kiritsu.multiparticlesim.tp4.force.ForceEmitter;

public class AreaForcesUpdater extends Block<Area> {

	@Override
	protected void exec(Area input) {
		for (Particle particle : input.particles()) {
			particle.setAppliedForce(Vectors2f.ZERO());
		}
		for (ForceEmitter force : input.forces()) {
			force.apply(input);
		}
	}

}
