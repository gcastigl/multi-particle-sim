package ar.com.kiritsu.multiparticlesim.tp4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.block.Block;
import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.ElapsedTimeIsGreaterThan;
import ar.com.kiritsu.multiparticlesim.IncrementElapsedTime;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.SimulationApp;
import ar.com.kiritsu.multiparticlesim.Spring;
import ar.com.kiritsu.multiparticlesim.Spring.FixedSpringPoint;
import ar.com.kiritsu.multiparticlesim.Spring.ParticleSpringPoint;
import ar.com.kiritsu.multiparticlesim.tp4.force.SpringForce;
import ar.com.kiritsu.multiparticlesim.tp4.logic.AreaForcesUpdater;
import ar.com.kiritsu.multiparticlesim.tp4.logic.GearPredictorStep;

import com.google.common.base.Function;
import com.google.common.base.Suppliers;

public class DampedPunctualOscillatorApp extends SimulationApp {

	public static void main(String[] args) throws SlickException {
		AppGameContainer appContainer = new AppGameContainer(new DampedPunctualOscillatorApp());
		appContainer.setUpdateOnlyWhenVisible(false);
		appContainer.setDisplayMode(800, 700, false);
		appContainer.start();
	}

	private static final float TIME_STEP = 1 / 1000f;

	public DampedPunctualOscillatorApp() {
		super("Oscilador puntual amortiguado");
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		super.init(container);
		renderer().setScale(10).setPostRender(new ShowEnergyOnArea());
		final int order = 5;
		final FileWriter writer;
		try {
			writer = new FileWriter(new File("./src/main/resources/tp4/data.txt"));
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		simulation()
			.setCutCondition(new ElapsedTimeIsGreaterThan(70f))
			.setInitialState(new Area())
			.onStart(new Block<Area>() {
				@Override 
				protected void exec(Area input) { setup(input, order); }
			})
			.onStep(new GearPredictorStep(TIME_STEP))
//			.onStep(new BeemanStep(TIME_STEP))
//			.onStep(new VerletMod(TIME_STEP))
//			.onStep(new Function<Area, Area>() {
//				@Override
//				public Area apply(Area input) {
//					Spring spring = input.springs().get(0);
//					float m = spring.destination().mass();
//					float k = spring.k();
//					float fi = spring.gamma();
//					float elapsedTime = input.elapsedTime();
//					float c1 = (float) Math.exp(-spring.gamma() * elapsedTime / (2 * m));
//					float c2 = k / m - (fi * fi) / (4 * m * m);
//					float c2_ = (float) Math.cos(Math.sqrt(c2) * elapsedTime);
//					float r = c1 * c2_;	//Math.abs(
//					try {
////						writer.write(r + "\n");
//						writer.write((r - spring.destination().position().x) + "\n");
//					} catch (IOException e) { 
//						throw new IllegalStateException();
//					}
//					return input;
//				}
//			})
			.onStep(new AreaForcesUpdater())
			.onStep(new IncrementElapsedTime(Suppliers.ofInstance(TIME_STEP)))
			.onEnd(new Function<Area, Area>() {
				@Override
				public Area apply(Area input) {
					try {
						writer.close();
					} catch (IOException e) {
						throw new IllegalStateException();
					}
					return input;
				}
			})
		;
	}

	private void setup(Area input, int order) {
		float p0m = 20;
		Particle p0 = new Particle(0, 1, p0m, order);
//		Particle p1 = new Particle(1, 1, 20, order);
//		Particle p2 = new Particle(1, 1, 20, order);
//		Particle p3 = new Particle(1, 1, 80, order);
		float gamma = 0;
		input.addParticle(
			p0.setPosition(new Vector2f(15, 15))
//			, p1.setPosition(new Vector2f(17, 17))
//			, p2.setPosition(new Vector2f(17, 20))
//			, p3.setPosition(new Vector2f(14, 20))
		);
		FixedSpringPoint fixedPoint = new FixedSpringPoint(new Vector2f(10f, 10f));
		input.springs().add(new Spring(2000, gamma, fixedPoint, new ParticleSpringPoint(p0)));
//		input.springs().add(new Spring(1000, new ParticleSpringPoint(p0), new ParticleSpringPoint(p1)));
//		input.springs().add(new Spring(1000, new ParticleSpringPoint(p2), new ParticleSpringPoint(p1)));
//		input.springs().add(new Spring(1000, new ParticleSpringPoint(p3), new ParticleSpringPoint(p0)));
//		input.springs().add(new Spring(10, fixedPoint, new ParticleSpringPoint(p1)));
		
//		input.forces().add(new GravityForce());
		input.forces().add(new SpringForce());
	}

}
