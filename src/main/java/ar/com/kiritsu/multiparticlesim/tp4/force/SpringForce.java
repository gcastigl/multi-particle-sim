package ar.com.kiritsu.multiparticlesim.tp4.force;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;
import ar.com.kiritsu.multiparticlesim.Spring;
import ar.com.kiritsu.multiparticlesim.Vectors2f;

public class SpringForce extends ForceEmitter {

	private static final Vector2f VERSOR_CACHE = new Vector2f();

	@Override
	protected void exec(Area input) {
		for (Spring spring : input.springs()) {
			applyForces(spring);
		}
	}

	public void applyForces(Spring spring) {
		Vector2f versor = versor(spring.destination().position(), spring.origin().position());
		Vector2f force = versor.scale(-spring.k() * spring.steching());

		Vector2f f1 = new Vector2f(spring.origin().velocity()).scale(-spring.gamma());
		spring.origin().applyForce(new Vector2f(force).sub(f1).scale(-1));
		
		Vector2f f2 = new Vector2f(spring.destination().velocity()).scale(spring.gamma());
		spring.destination().applyForce(new Vector2f(force).sub(f2));
	}

	private Vector2f versor(Vector2f v1, Vector2f v2) {
		Vectors2f.versor(v2, v1, VERSOR_CACHE);
		return VERSOR_CACHE;
	}

	@Override
	public EnergyValue energy(Area input) {
		float potential = 0;
		for (Spring spring : input.springs()) {
			float steching = spring.steching();
			potential += spring.k() * steching * steching / 2;
		}
		return new EnergyValue(0, potential);
	}
}
