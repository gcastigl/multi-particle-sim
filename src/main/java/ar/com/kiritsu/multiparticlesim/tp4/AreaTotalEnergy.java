package ar.com.kiritsu.multiparticlesim.tp4;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.tp4.force.ForceEmitter;

import com.google.common.base.Function;

public class AreaTotalEnergy implements Function<Area, EnergyValue> {

	@Override
	public EnergyValue apply(Area input) {
		EnergyValue totalEnergy = new EnergyValue(0, 0);
		for (ForceEmitter force : input.forces()) {
			totalEnergy.add(force.energy(input));
		}
		for (Particle particle : input.particles()) {
			float v = particle.velocity().length();
			float cinametic = particle.mass() * v * v / 2;
			totalEnergy.addCinematic(cinametic);
		}
		return totalEnergy;
	}

}
