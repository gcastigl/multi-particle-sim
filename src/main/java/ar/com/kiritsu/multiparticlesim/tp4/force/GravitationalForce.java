package ar.com.kiritsu.multiparticlesim.tp4.force;

import java.util.Collection;

import org.newdawn.slick.geom.Vector2f;

import ar.com.kiritsu.multiparticlesim.Area;
import ar.com.kiritsu.multiparticlesim.EnergyValue;
import ar.com.kiritsu.multiparticlesim.Particle;
import ar.com.kiritsu.multiparticlesim.Vectors2f;

public class GravitationalForce extends ForceEmitter {

	private static final double G = 6.69 * 10E-11;
	private static final Vector2f eij = new Vector2f();

	@Override
	protected void exec(Area input) {
		for (Particle particle : input.particles()) {
			applyForce(particle, input.particles());
		}
	}

	public void applyForce(final Particle subject, Collection<Particle> others) {
		final Vector2f result = new Vector2f();
		for (Particle other : others) {
			if (other == subject) {
				continue;
			}
			float distanceSq = subject.position().distanceSquared(other.position());
			float r = subject.radius() + other.radius();
			if (distanceSq <= r * r) {
				// System.out.println("Particulas colisionando... ignrando calculo de fuerza gravitacional");
				return;
			}
			double c = (G * other.mass() * subject.mass()) / distanceSq;
			result.add(eij(subject, other).scale((float) c));
		}
		subject.appliedForce().add(result);
	}

	private Vector2f eij(Particle p1, Particle p2) {
		Vectors2f.versor(p1.position(), p2.position(), eij);
		return eij;
	}
	
	@Override
	public EnergyValue energy(Area input) {
		float potential = 0;
		for (Particle pi : input.particles()) {
			float v = pi.velocity().length();
			potential += (pi.mass() * v * v) / 2;
			for (Particle pj : input.particles()) {
				if (pj != pi) {
					float distance = pi.position().distance(pj.position());
					potential += GravitationalForce.G * pi.mass() * pj.mass() / distance;
				}
			}
		}
		return new EnergyValue(0, potential);
	}

}
