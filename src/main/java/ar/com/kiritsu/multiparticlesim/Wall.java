package ar.com.kiritsu.multiparticlesim;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

import com.google.common.base.Optional;

public class Wall {

	private Line _bounds;
	private Optional<Vector2f> _traspassableDir = Optional.absent();

	public Wall() {
	}

	public Wall(Line bounds) {
		_bounds = bounds;
	}

	public Wall setBounds(Line bounds) {
		_bounds = bounds;
		return this;
	}

	public Line bounds() {
		return _bounds;
	}

	public Wall setTraspassableDir(Vector2f traspassableDir) {
		_traspassableDir = Optional.of(traspassableDir);
		return this;
	}

	public Optional<Vector2f> traspassableDir() {
		return _traspassableDir;
	}

	@Override
	public String toString() {
		return "wall: " + bounds();
	}
}
