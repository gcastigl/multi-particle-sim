package ar.com.kiritsu.multiparticlesim;

import com.google.common.base.Predicate;

public class ElapsedTimeIsGreaterThan implements Predicate<Area> {

	private final float _time;

	public ElapsedTimeIsGreaterThan(float time) {
		_time = time;
	}

	@Override
	public boolean apply(Area input) {
		return input.elapsedTime() > _time;
	}

}
